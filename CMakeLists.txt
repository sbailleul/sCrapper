cmake_minimum_required(VERSION 3.15)
project(sCrapper C)

set(CMAKE_C_STANDARD 99)

FIND_PACKAGE(PkgConfig REQUIRED)
FIND_PACKAGE(Threads REQUIRED)

SET(SRC_DIR src)
SET(INCLUDE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include)
file(GLOB RESSOURCES "${CMAKE_CURRENT_SOURCE_DIR}/ressources/*")

PKG_CHECK_MODULES(CURL REQUIRED libcurl)
INCLUDE_DIRECTORIES(${CURL_INCLUDE_DIRS} ${INCLUDE_DIR} )
LINK_DIRECTORIES(${CURL_LIBRARY_DIRS})
ADD_DEFINITIONS(${CURL_CFLAGS_DIRS})

foreach(file ${RESSOURCES})
    get_filename_component(fileName ${file} NAME)
    CONFIGURE_FILE(${file} ${CMAKE_CURRENT_BINARY_DIR}/ressources/${fileName})
endforeach(file)

add_executable(sCrapper ${SRC_DIR}/main.c  src/fileHandler.c  ${SRC_DIR}/fileReader.c  src/stringUtils.c src/scrapConfig.c src/curlHandler.c   src/multiThreadHandler.c src/actionTree.c   src/urlHandler.c  src/genericUtils.c src/fileWriter.c   src/actionThreadHandler.c src/timeHandler.c )
TARGET_LINK_LIBRARIES(sCrapper ${CURL_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT})

