//
// Created by vagahbond on 11/12/19.
//
#include <time.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

#include "scrapper/stringUtils.h"
#include "scrapper/timeHandler.h"


char* getDateTime(time_t stamp, TimeDef def, pthread_mutex_t* transformTimeMutex){

    struct tm *ptm = NULL;
    char* res = NULL;


    if(transformTimeMutex != NULL){
        pthread_mutex_lock(transformTimeMutex);
    }
    ptm = localtime(&stamp);
    if (def == TIME || def == BOTH){
        char *time = NULL;
        if ((time = malloc(128* sizeof(char))) == NULL){
            free(res);
            if(transformTimeMutex != NULL){
                pthread_mutex_unlock(transformTimeMutex);
            }
            return  NULL;
        }
        sprintf(time, "%02d:%02d:%02d",  ptm->tm_hour, ptm->tm_min, ptm->tm_sec );
        strCatAlloc(&res, time, RIGHT);
        free(time);
    }
    if (def == DATE || def == BOTH){
        char* month[12] = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
        char* week[7] = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
        char *date = NULL;
        if ((date = malloc(128* sizeof(char))) == NULL){
            free(res);
            if(transformTimeMutex != NULL){
                pthread_mutex_unlock(transformTimeMutex);
            }
            return  NULL;
        }
        sprintf(date, "%s %02d %s %04d",  week[ptm->tm_wday], ptm->tm_mday, month[ptm->tm_mon], ptm->tm_year+1900);
        strCatAlloc(&res, date, LEFT);
        free(date);
    }
    if(transformTimeMutex != NULL){
        pthread_mutex_unlock(transformTimeMutex);
    }
    return  res;

}