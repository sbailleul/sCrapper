#include <stdio.h>
#include <curl/curl.h>
#include <locale.h>
#include <stdlib.h>
#include <scrapper/genericUtils.h>
#include <string.h>
#include "scrapper/curlHandler.h"
#include "scrapper/fileHandler.h"
#include "scrapper/stringUtils.h"



CurlRequest *initCurlRequest(char *url, char *acceptedMimes, FILE *dstFile){

    CurlRequest *curlRequest;

    if( (curlRequest=malloc(sizeof(CurlRequest))) == NULL ){
        createErrorReport("Memory allocation error : ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }
    curlRequest->url=NULL;
    curlRequest->dstFile=NULL;
    curlRequest->acceptedMimesArr=NULL;
    curlRequest->acceptedMimesArrSize=0;

    if(acceptedMimes != NULL){
        curlRequest->acceptedMimesArr = splitStringOnSeparator(acceptedMimes,&curlRequest->acceptedMimesArrSize,",",ALL,DESTROY);
    }

    if((curlRequest->url = strCpyAlloc(url)) == NULL || (curlRequest->dstFile=dstFile) == NULL){
        freeCurlRequest((void**)&curlRequest);
    }

    return curlRequest;
}

char freeCurlRequest(void ** untypedCurlRequest){

    CurlRequest * curlRequest = NULL;

    if(untypedCurlRequest == NULL || *untypedCurlRequest == NULL){
        return 0;
    }
    curlRequest = (CurlRequest*) *untypedCurlRequest;

    if(curlRequest->url!=NULL){
        inlineFree(1,&curlRequest->url);
    }
    if(curlRequest->dstFile!=NULL){
        fclose(curlRequest->dstFile);
        curlRequest->dstFile=NULL;
    }
    if(curlRequest->acceptedMimesArr!=NULL){
        recursFree((void***)&curlRequest->acceptedMimesArr,&curlRequest->acceptedMimesArrSize,1);
    }

    free(curlRequest);
    curlRequest=NULL;

    return 1;
}


char* curlConnect(CurlRequest *curlRequest){


    CURL *curl = curl_easy_init();
    struct curl_slist *headers = NULL;
    char *contentType = NULL;
    char *response = NULL;

    CURLcode result;
    curlSetOpt(curlRequest, curl, headers);

    if((result = curl_easy_perform(curl)) != CURLE_OK){
        createErrorReport("curl_easy_perform() fail on url : %s,  error : %s\n",__FILE__,__LINE__,__DATE__,__TIME__,curlRequest->url, curl_easy_strerror(result));
        curl_easy_cleanup(curl);
        curl_slist_free_all(headers);
        return NULL;
    }

    if (curl_easy_getinfo(curl, CURLINFO_CONTENT_TYPE, &contentType) != CURLE_OK ){
        response = strCpyAlloc("Unknown");
    }
    else {
        int len = (int) strlen(contentType);
        response = splitStringOnSeparator(strCpyAlloc(contentType), &len ,";",LEFT, DESTROY  )[0];
    }

    if(curlRequest->acceptedMimesArr != NULL){
        if(findStringInStringArr(contentType,curlRequest->acceptedMimesArr, curlRequest->acceptedMimesArrSize)<0){
           response = NULL;
       }
    }

    curl_easy_cleanup(curl);
    curl_slist_free_all(headers);
    return response;
}




size_t curlToFile(void *ptr, size_t size, size_t nmemb, FILE *dstStream) {
    return fwrite(ptr, size, nmemb, dstStream);
}



char curlSetOpt(CurlRequest *curlRequest, CURL *curl, struct curl_slist *headers){


    //URL de la requête
    curl_easy_setopt(curl, CURLOPT_URL, curlRequest->url);
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L); //follow redirections

    //Ne vérifie pas l'authenticité du certificat envoyé par le serveur quand la valeur est à 0, vérifie quand la valeur est à 1.
    //J'ai mis la valeur à 0 car *curl stopait la connexion avec l'API openfoodfacts lorsqu'une vérification était effectuée.
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);

    //Création d'un header vide
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers); // add struct defining the url, options...

    //prompts the server to compress its response using zlib algorithm
    curl_easy_setopt(curl, CURLOPT_ACCEPT_ENCODING, "deflate");
    //callback function defining what to do with the result
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curlToFile);
    //defines maximum time before timeout
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, 10);
    //file to which the call back must be executed
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, curlRequest->dstFile);

    return 1;
}
