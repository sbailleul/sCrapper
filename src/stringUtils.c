//
// Created by sbailleul on 10/13/19.
//
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <scrapper/genericUtils.h>
#include <scrapper/fileHandler.h>
#include "scrapper/stringUtils.h"


char * rtrim(char **string, char *trimChar, Direction trimDir){

    int size = 0;
    int start=0;
    int end=0;

    if(string==NULL || *string == NULL){
        return NULL;
    }
    size = strlen(*string);
    end = size-1;

    while((trimDir==LEFT || trimDir==ALL) && charsCmp((*string)[start],trimChar) && start < size){
        start++;
    }
    while((trimDir==RIGHT || trimDir==ALL) && charsCmp((*string)[end],trimChar) && end>0){
        end--;
    }

    return extractSubString(string,start,end);
}

char *extractStringOnDelimiters(char **string, char *delimiters, Direction extractDir){

    int size = 0;
    int startCnt=0;
    int endCnt= 0;
    if(string == NULL || *string == NULL){
        return NULL;
    }
    size = strlen(*string);

    endCnt = extractDir ==  LEFT ? 0 : size-1;
    startCnt = extractDir == RIGHT ? size-1 : 0;

    while(extractDir==LEFT && !charsCmp((*string)[endCnt], delimiters) && endCnt < size){
        endCnt++;
    }
    while(extractDir==RIGHT && !charsCmp((*string)[startCnt], delimiters) && startCnt > 0){
        startCnt--;
    }

    endCnt = endCnt != size - 1 ? endCnt - 1 : endCnt;

    return extractSubString(string, startCnt, endCnt);
}

char *extractSubString(char **string, int start, int end){

    char *subString=NULL;

    if(string == NULL || *string == NULL){
        return NULL;
    }
    if((start<0 && end>=strlen(*string))){
        return *string;
    }
    if((subString=calloc(end-start+2,sizeof(char)))==NULL){
        return NULL;
    }

    strncpy(subString,(*string)+start,(end-start+1));
    subString[strlen(subString)]='\0';
    strcpy(*string,subString);
    free(subString);

    return *string;
}

char *strCpyAlloc(char *srcString){

    char *dstString = NULL;
    if(srcString==NULL){
        return NULL;
    }
    else if((dstString=calloc(((int)strlen(srcString)+1), sizeof(char)))==NULL){
        createErrorReport("Memory allocation error : ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }

    return strcpy(dstString,srcString);
}


char charsCmp(char srcChar, char *compChars){

    if(compChars == NULL){
        return 0;
    }
    for(int i=0 ; i<strlen(compChars); i++){
        if(compChars[i]==srcChar){
            return compChars[i];
        }
    }
    return 0;
}

char replaceWordByWord(char **string, char *wordToReplace, char *replaceWord){

    char *foundString = NULL;
    char *tmpString = NULL;
    char stopFlag = 0;
    int checkCounter = 0;
    int wordToReplaceCnt = 0;
    int wordToReplaceSize = 0;
    int replaceWordSize = 0;

    if(string == NULL || *string == NULL || wordToReplace == NULL || replaceWord == NULL|| strstr(*string, wordToReplace) == NULL){
        return 0;
    }
    wordToReplaceSize= strlen(wordToReplace);
    replaceWordSize= strlen(replaceWord);

    while((wordToReplaceCnt=strStrConsecutiveString(*string+checkCounter,wordToReplace))){

        if(strstr(*string+checkCounter,wordToReplace) == *string) {
            strcpy(*string, *string + wordToReplaceSize * wordToReplaceCnt);
            for (int i = 0; i < wordToReplaceCnt; i++) {
                strCatAlloc(string, replaceWord, LEFT);
            }
        } else if(checkCounter>0) {
            tmpString = strCpyAlloc(*string + checkCounter + wordToReplaceSize * wordToReplaceCnt);
            stopFlag = strlen(tmpString) == 0 ? 1 : 0 ;

            for (int i = 0; i < wordToReplaceCnt; i++) {
                strCatAlloc(&tmpString, replaceWord, LEFT);
            }
            if((*string = realloc(*string , sizeof(char)*(checkCounter + strlen(tmpString) +1))) == NULL){
                createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);

            }
            strcpy(*string+checkCounter,tmpString);
            free(tmpString);
        }
        if(stopFlag){
            break;
        }
        if((foundString = strstr(*string+checkCounter+(replaceWordSize * wordToReplaceCnt),wordToReplace))!=NULL){
            checkCounter = foundString - *string;
        } else {
            break;
        }
    }


    return 1;
}


int removeCharsInString(char *string, char *charsToRemove){

    int size = 0;
    int rmCounter = 0;

    if(string==NULL || charsToRemove == NULL){
        return -1;
    }

    size = strlen(string);

    for(int i=0 ; i<size ; i++){

        int j = -1;
        while(j++, charsCmp(string[i+j],charsToRemove) && i+j < size);

        if(j){
            strcpy(string+i,string+i+j);
            rmCounter += j;
        }
    }

    return rmCounter;
}

char **getFormatArr(const char * formats, int *formatSize){

    char *workFormat;
    char **formatArr ;

    if(formats == NULL || formatSize == NULL){
        return NULL;
    }
    if((workFormat=malloc(sizeof(char)*(strlen(formats) + 1))) == NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }

    strcpy(workFormat, formats);

    if((formatArr=splitStringOnSeparator(workFormat, formatSize, "%", ALL, DESTROY))==NULL){
        free(workFormat);
        return NULL;
    }
    free(workFormat);

    return formatArr;
}




int strStrConsecutiveString(char *string , char *searchedString){

    char *curString;
    char *prevString;
    int consecCounter = 1;

    if(string == NULL || searchedString == NULL || strstr(string,searchedString) == NULL){
        return 0;
    }


    do{
        prevString=strstr(string,searchedString);
        curString=strstr(prevString+strlen(searchedString),searchedString);
        if((curString-prevString)==strlen(searchedString)){
            consecCounter++;
        }
    } while((prevString-curString)==strlen(searchedString));


    return consecCounter;
}
int strTokWord(char **foundPt, char *string , char *wordSeparator, char *erasedChar, int *erasedPos){

    char *tmpFoundPt=NULL;
    int separatorCnt=0;

    if((*foundPt)==NULL && *erasedPos==-1){
        tmpFoundPt = strstr(string,wordSeparator);
        if(tmpFoundPt==string){ // Control if tmpFoundPt is located on the start of the string
            (*foundPt)=string;
            separatorCnt = strStrConsecutiveString(*foundPt,wordSeparator);
            (*foundPt) += separatorCnt * strlen(wordSeparator);//Move pointer on string when is different of wordSeparator
        } else if(tmpFoundPt>string || tmpFoundPt == NULL ) {
            *foundPt = string;
        }
    }
    if(*foundPt!=NULL){
        if(*erasedPos != -1){
            (*foundPt)[*erasedPos]=*erasedChar;//Replace end string char by last erased char to reform origin string
            (*foundPt)=strstr((*foundPt),wordSeparator);//Search on last found string next separator
            if(*foundPt==NULL){//End of the research
                return 0;
            }
            separatorCnt = strStrConsecutiveString(*foundPt,wordSeparator) ; //Look how much consecutive time separator is located from current found string
            (*foundPt) += separatorCnt * strlen(wordSeparator); //Increase position of current found string
        }
        tmpFoundPt = strstr((*foundPt),wordSeparator);
        if(tmpFoundPt != NULL){
            *erasedPos= strlen((*foundPt)) - strlen(tmpFoundPt);
        } else {
            *erasedPos= strlen((*foundPt));
        }
        *erasedChar=(*foundPt)[*erasedPos];//Stock char which will be erased by end string char '\0'
        (*foundPt)[*erasedPos]='\0';//Cut string with end string char '\0'
    }
    return separatorCnt;

}

char **splitStringOnSeparator(char *string, int *size, const char *separator, Direction direction, SeparatorDirective directive){

    char *buf=NULL;
    char *preBuf=NULL;
    char **splitArr=NULL;
    char erasedChar='\0';
    int erasedCharPos=-1;
    int separatorCnt=0;
    int  i= directive==KEEP && direction == RIGHT  ? -1 : 0;
    int newStringCnt=0;

    if(string == NULL  || separator == NULL){
        return NULL;
    }

    separatorCnt=strTokWord(&buf,string,(char*)separator,&erasedChar,&erasedCharPos);

    while(buf!=NULL || separatorCnt != 0 || preBuf != NULL){

        char *newString = NULL;

        if(directive ==  KEEP && (direction == LEFT || direction == RIGHT)){
            if(direction == RIGHT){
                if(i>=0){
                    keepSeparatorOnSplitedString( &newString, separator, direction, preBuf,separatorCnt);
                    free(preBuf);
                    preBuf=NULL;
                }
                preBuf=strCpyAlloc(buf);
            } else if (direction == LEFT){
                keepSeparatorOnSplitedString( &newString, separator, direction, buf,separatorCnt);
            }
        } else {
            if((newString=strCpyAlloc(buf))==NULL){
                recursFree((void***)&splitArr,&newStringCnt,1);
                break;
            }
            rtrim(&newString," ",ALL);
        }
        if(newString != NULL && strlen(newString)>0){
            if((splitArr=realloc(splitArr,sizeof(char*)*(newStringCnt+1)))==NULL){
                createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
                break;
            }
            splitArr[newStringCnt]=strCpyAlloc(newString);
            newStringCnt++;
        }
        if(newString!=NULL){
            free(newString);
        }
        i++;
        separatorCnt=strTokWord(&buf,string,(char*)separator,&erasedChar,&erasedCharPos);
    }
    if(size!=NULL){
        *size=newStringCnt;
    }
    return splitArr;
}


char *strCatAlloc(char **dstString, char *catString, Direction dir){

    char *tmpString =NULL ;
    int dstStringSize= 0;
    char nullFlag = 0;

    if( dstString == NULL || *dstString == NULL ){
        dstStringSize = 1;
        nullFlag=1;
    } else {
        dstStringSize = strlen(*dstString);
    }

    if((tmpString=calloc(dstStringSize+1,sizeof(char)))==NULL){
        return NULL;
    }
    if((*dstString=realloc(*dstString,sizeof(char)*(dstStringSize+strlen(catString)+1)))==NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        free(tmpString);
        return NULL;
    }
    if(nullFlag){
        (*dstString)[0]='\0';
    }

    if(dir == LEFT){
        strcat(strcpy(*dstString,catString),strcpy(tmpString,*dstString));
    } else {
        strcat(*dstString,catString);
    }

    free(tmpString);
    return *dstString;
}


char * strMergeFromArr(char **stringArr, int start, int end, char **dstString){

    for(int i=start ; i<end ; i++){
        char *subStr = stringArr[i];
        strCatAlloc(dstString,subStr,RIGHT);
    }
    return *dstString;

}

char *repeatString(char *string, int count){

    char *newString;

    if(string == NULL){
        return NULL;
    }
    if((newString=calloc(((strlen(string)*count)+1),sizeof(char)))==NULL){
        return NULL;
    }

    for(int i=0 ; i<count ;i++){
        strcat(newString,string);
    }
    return newString;
}

char keepSeparatorOnSplitedString(char **substring, const char *separator, Direction direction , char *splitString, int separatorCnt){

    char * repeatedString=NULL;

    if((repeatedString = repeatString((char*)separator,separatorCnt))==NULL){
        *substring=strCpyAlloc(splitString);
        return 1;
    } else {
        *substring=strCpyAlloc(splitString);
        strCatAlloc(substring,repeatedString,direction);
        free(repeatedString);
    }

    return 1;
}

unsigned int getStringArrSize(char **stringArr, int start, int end){

    unsigned int stringArrSize = 0;
    if(stringArr == NULL){
        return 0;
    }
    for(int i=start ; i<end; i++){
        if(stringArr[i] != NULL){
            stringArrSize += strlen(stringArr[i]);
        }
    }
    return stringArrSize;
}

int findStringInStringArr(char *stringToFind, char **stringArr, int stringArrSize){

    if(stringToFind == NULL || stringArr == NULL || stringArrSize <= 0){
        return -1;
    }

    for(int i=0; i<stringArrSize ; i++){
        if(strstr(stringToFind, stringArr[i]) !=NULL ){
            return i;
        }
    }
    return -1;
}

