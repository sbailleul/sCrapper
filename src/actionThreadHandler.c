//
// Created by sbailleul on 11/7/19.
//
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <scrapper/stringUtils.h>
#include <string.h>
#include "scrapper/actionThreadHandler.h"
#include "scrapper/multiThreadHandler.h"
#include "scrapper/curlHandler.h"
#include "scrapper/urlHandler.h"
#include "scrapper/genericUtils.h"
#include "scrapper/actionTree.h"
#include "scrapper/fileReader.h"

char checkActThreadRunning(ActionThreadCentralizer *actThreadCentral, ActionThread *actThread, int curThreadWorkingFlag){

    char endFlag = 1;
    TaskThread *tskThread = NULL;

    if(actThreadCentral == NULL || actThread == NULL){
        return 0;
    }

    pthread_mutex_lock(&actThreadCentral->checkActThreadMutex);

    if(actThread->workFlag != curThreadWorkingFlag){
        actThreadCentral->runningActThreadCnt += curThreadWorkingFlag ? 1 : -1;
        actThread->workFlag = curThreadWorkingFlag;
    }
    if(actThreadCentral->runningActThreadCnt == actThreadCentral->actThreadArrSize * -1 && (tskThread = actThread->taskThread) != NULL){

        pthread_mutex_lock(&tskThread->checkTaskThreadMutex);
        tskThread->runningActThreadCnt--;
        if( !actThread->timed){
            pthread_cond_broadcast(&tskThread->tskThreadRunIntervalCond);
        }
        pthread_mutex_unlock(&tskThread->checkTaskThreadMutex);
        endFlag = 0;
    }

    pthread_mutex_unlock(&actThreadCentral->checkActThreadMutex);

    return endFlag;
}

char incrementVersionInfoArrays( TaskThread *tskThread, ActionThreadCentralizer* actCentral, char* fileType, char* url){

    VersionInfo* ver = actCentral->versionInfo;
    int keyIndex = 0;
    if ((keyIndex= findStringInStringArr(fileType, ver->typeKeysArray,ver->countPerTypeArrSize ))<0){
        if((ver->typeKeysArray = realloc(ver->typeKeysArray, sizeof(char*)*(ver->countPerTypeArrSize+1))) == NULL){
            createErrorReport("Memory allocation error for url :%s :  ",__FILE__,__LINE__,__DATE__,__TIME__, url);
            return 0;
        }
        if((ver->typeCounterArray = realloc(ver->typeCounterArray, sizeof(int)*(ver->countPerTypeArrSize+1))) == NULL){
            createErrorReport("Memory allocation error for url :%s :  ",__FILE__,__LINE__,__DATE__,__TIME__, url);
            return 0;
        }
        if ((ver->typeKeysArray[ver->countPerTypeArrSize] = strCpyAlloc(fileType)) == NULL){
            ver->countPerTypeArrSize++;
            recursFree((void ***)&ver->typeKeysArray, &ver->countPerTypeArrSize, 1);
            free(ver->typeCounterArray);
        }
        ver->typeCounterArray[ver->countPerTypeArrSize] = 1;
        ver->countPerTypeArrSize++;
    } else {
        ver->typeCounterArray[keyIndex]++;
    }

    fprintf(ver->versionLogFile,"     Instance %d of task : %s, fetched URL : %s | Type : %s\n",tskThread->instanceID, tskThread->task->name, url, fileType);

    ver->totalFilesCount++;

    return 1;
}





ActionTree *transformActionTreeBucket(ActionTree ***actTreeBucket, ActionTree *pushedActionTree, unsigned long *actTreeBucketSize, pthread_mutex_t *transformBucketMutex){


    ActionTree *poppedActTree = NULL;


    pthread_mutex_lock(transformBucketMutex);

    if(pushedActionTree == NULL){
        poppedActTree = popElementFromPointerArray((void ***) actTreeBucket, actTreeBucketSize, sizeof(ActionTree *));
    } else {
        pushElementInPointerArray((void ***) actTreeBucket, actTreeBucketSize, (void *) pushedActionTree, sizeof(ActionTree *));
    }

    pthread_mutex_unlock(transformBucketMutex);

    return poppedActTree;
}

char transformActionTree(ActionThread *actThread, ActionTree *actTree){

    ActionThreadCentralizer *actCentralizer = NULL;
    FILE *fileToWrite = NULL;
    char *urlToRequest = NULL;
    ScrapConfig *scrapConfig = NULL;

    if(actThread == NULL || actTree == NULL || (actCentralizer = actThread->actCentral) == NULL || actThread->multiThread == NULL || (scrapConfig = actThread->multiThread->scrapConfig) == NULL){
        return 0;
    }

    pthread_mutex_lock(&actCentralizer->transformActTreeMutex);
    if ((fileToWrite = writeActionTreeToFileSystem(actTree, actCentralizer->action->directory) ) == NULL){
        createErrorReport("Writing curl destination file error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        pthread_mutex_unlock(&actCentralizer->transformActTreeMutex);
        return 0;
    }

    if((urlToRequest = getResourceLocatorByActionTree(actTree,NULL)) == NULL){
        createErrorReport("Getting curl url to request error from action tree error : ",__FILE__,__LINE__,__DATE__,__TIME__);
        pthread_mutex_unlock(&actCentralizer->transformActTreeMutex);
        return 0;
    }
    pthread_mutex_unlock(&actCentralizer->transformActTreeMutex);

    return treatCurlResponse(actCentralizer, actThread->taskThread, actTree, scrapConfig, fileToWrite, urlToRequest);
}


char treatCurlResponse(ActionThreadCentralizer *actCentralizer, TaskThread *tskThread, ActionTree *actTree, ScrapConfig *scrapConfig, FILE *curlResultFile, char *requestedUrl ){

    CurlRequest *curlRequest = NULL;
    Option *opt = NULL;
    Action *action = NULL;
    char *contentTypes = NULL;
    char* curlRes=NULL;

    if(actCentralizer == NULL || actTree == NULL || scrapConfig == NULL || curlResultFile == NULL || requestedUrl == NULL || (action = actCentralizer->action) == NULL){
        return 0;
    }

    if((opt = getOptionByKey(CONTENT_TYPES, action->optionsArr, action->optArrSize)) != NULL){
        contentTypes = opt->value->string;
    }
    if((curlRequest = initCurlRequest(requestedUrl, contentTypes, curlResultFile)) == NULL){
        createErrorReport("Cannot create curlRequest for  url : %s : ", __FILE__, __LINE__, __DATE__, __TIME__, requestedUrl);
        free(requestedUrl);
        fclose(curlResultFile);
        return 0;
    }

    if((curlRes = curlConnect(curlRequest)) != NULL){
        addCurlResponseToActionTree(curlRequest, scrapConfig->tagAttributeArr, scrapConfig->tagAttributeArrSize, actCentralizer);
        //increment versioninfo types arrays
        pthread_mutex_lock(&actCentralizer->transformVersionInfoMutex);
        if (actCentralizer->versionInfo != NULL && tskThread != NULL){
            incrementVersionInfoArrays(tskThread, actCentralizer,curlRes, curlRequest->url);
        }
        pthread_mutex_unlock(&actCentralizer->transformVersionInfoMutex);
    }else{
        if(curlRes == NULL){
            char *pathToRemove = getResourceLocatorByActionTree(actTree, action->directory);
            createErrorReport("Error on curl connection for  url : %s : ", __FILE__, __LINE__, __DATE__, __TIME__, requestedUrl);
            remove(pathToRemove);
            free(pathToRemove);
        }
    }
    freeCurlRequest((void**)&curlRequest);

    return 1;
}


int addCurlResponseToActionTree(CurlRequest *curlRequest, TagAttribute **tagAttributeArr, int tagAttributeArrSize, ActionThreadCentralizer *actCentral){

    ActionTree *newActTree = NULL;
    Option *opt = NULL;
    char **urlArr = NULL;
    int urlArrSize = 0;
    int  maxDepth = -1;
    int addedActionTreeCounter = 0;

    if((urlArr = getUrlArrFromHtmlDocument(curlRequest->dstFile,&urlArrSize,tagAttributeArr,tagAttributeArrSize))!=NULL){

        opt = getOptionByKey(MAX_DEPTH, actCentral->action->optionsArr, actCentral->action->optArrSize);
        maxDepth = opt != NULL ? opt->value->integer : -1 ;

        for(int i=0; i<urlArrSize ;i++){

            char *absoluteUrl = getAbsoluteUrlFromRelativeUrl(NULL, curlRequest->url, urlArr[i]);
            if(absoluteUrl != NULL){

                rtrim(&absoluteUrl, "/", RIGHT);
                pthread_mutex_lock(&actCentral->transformActTreeMutex);
                newActTree = addActionTreeByAbsoluteUrl(actCentral->rootActTree, curlRequest->url, absoluteUrl, maxDepth );
                pthread_mutex_unlock(&actCentral->transformActTreeMutex);

                if(newActTree != NULL){
                    addedActionTreeCounter++;
                    transformActionTreeBucket(&actCentral->actTreeBucket, newActTree, &actCentral->actTreeBucketSize, &actCentral->transformBucketMutex);
                }
                free(absoluteUrl);
            }
        }
        recursFree((void***)&urlArr,&urlArrSize,1);
    }
    return addedActionTreeCounter;
}


