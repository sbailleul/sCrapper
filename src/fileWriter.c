//
// Created by sbailleul on 11/1/19.
//
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "scrapper/scrapConfig.h"
#include "scrapper/multiThreadHandler.h"
#include "scrapper/genericUtils.h"
#include "scrapper/fileWriter.h"
#include "scrapper/timeHandler.h"
#include "scrapper/stringUtils.h"
#include "scrapper/fileHandler.h"

FILE *createFileDir(char *path){

    char *tmpPath = NULL ;
    FILE *createdFile = NULL;
    char **pathArr = NULL;
    int pathArrSize = 0;


    if(path == NULL || (pathArr = splitStringOnSeparator(path,&pathArrSize,"/",RIGHT,KEEP)) == NULL){
        return NULL;
    }

    for(int i=1 ; i<pathArrSize ;i++){

        tmpPath = strMergeFromArr(pathArr, 0, i, &tmpPath);


        if(i<pathArrSize){
            struct stat st = {0};
            if (stat(tmpPath, &st) == -1) {
                mkdir(tmpPath, 0700);
            }
        }
        free(tmpPath);
        tmpPath = NULL;
    }
    createdFile = openFile(path,"w+");



    return createdFile;
}

char escapeStringToFileSystem(char ** string){

    replaceWordByWord(string,".","\\.");
    replaceWordByWord(string," ","\\ ");
}


FILE * initActionLog( Action* act)
{
    char* path =  NULL;
    char* header = NULL;
    FILE* actionFile = NULL;

    if((path = strCpyAlloc(act->directory)) == NULL|| (path = strCatAlloc(&path, "/actionLog.txt", RIGHT )) == NULL){
        return NULL;
    }
    actionFile = fopen(path, "r");

    if (actionFile == NULL){
        if ((header = malloc(sizeof(char)*500)) == NULL){
            free(path);
            return NULL;
        }
        sprintf(header, "--#This file keeps record of the versions for the action %s of the conf file, fetching %s. It was on created on %s_%s#-- \n\n",  act->name, act->url, __DATE__, __TIME__);
        if ((actionFile = createFileDir(path)) != NULL){
            fprintf(actionFile, "%s", header);
            fclose(actionFile);
        }
    }
    actionFile = fopen(path, "a+");

    free(header);
    free(path);

    return actionFile;
}

FILE *initVersionLog( Action* act, VersionInfo* ver){
    FILE* versionFile = NULL;
    char* path = NULL;
    char *startTime = NULL;
    char *startDate = NULL;

    if ((path = strCpyAlloc(act->directory)) == NULL|| (path = strCatAlloc(&path, "/versionLog.txt", RIGHT )) == NULL){
        return NULL;
    }
    if((versionFile = createFileDir(path)) == NULL){
        free(path);
        return NULL;
    }
    free(path);

    fprintf(versionFile, "Action %s performed by tasks : ", act->name);

    for (int i = 0; i< ver->tskArrSize; ++i){
        fprintf(versionFile, " %s ", ver->tskArr[i]->name);
    }

    startTime = getDateTime(ver->startStamp,TIME, NULL );
    startDate = getDateTime(ver->startStamp,DATE, NULL );

    if(startTime == NULL || startDate == NULL){
        inlineFree(2, &startTime, &startDate);
        fclose(versionFile);
        return NULL;
    }

    fprintf(versionFile, ". Started at %s, the %s, fetching %s\n________________________________\n", startTime, startDate, act->url);
    fprintf(versionFile,     "\n\nURLS recap: \n" );

    inlineFree(2, &startTime, &startDate);
    return versionFile;
}


char resumeVersionLog(VersionInfo *ver, char*  endTime, char* endDate ){

    fprintf(ver->versionLogFile,     "\n\nTypes recap: \n" );
    for (int i = 0; i< ver->countPerTypeArrSize; ++i)
    {
        fprintf(ver->versionLogFile, "     %d files of type %s\n", ver->typeCounterArray[i], ver->typeKeysArray[i]);

    }
    fprintf(ver->versionLogFile, "_____________________\nTotal number of files: %d\nEnded at %s on %s\n______________________________________\n\n", ver->totalFilesCount, endTime, endDate);

    return 1;
}

char resumeActionLog(VersionInfo *ver, Action *act, char *startTime, char *startDate, char*  endTime, char* endDate ){

    fprintf(ver->actionLogFile, "Action %s started at %s of %s, ended at %s of %s| Fetching URL : %s | %d files fetched |  by tasks : ",
            act->name, startTime , startDate, endTime, endDate, act->url, ver->totalFilesCount );

    for(int i=0 ; i < ver->tskArrSize ; i++){
        fprintf(ver->actionLogFile, " %s ", ver->tskArr[i]->name);
    }

    fprintf(ver->actionLogFile, "\n");
    return 1;
}

char resumeWriteLog(ActionThreadCentralizer* actCentral, LogType type ){

    VersionInfo *ver = NULL;
    Action *act = NULL;
    char * startTime = NULL;
    char * startDate = NULL;
    char * endTime = NULL;
    char * endDate = NULL;

    if (actCentral == NULL || (ver = actCentral->versionInfo) == NULL ||  (act = actCentral->action) == NULL || ver->actionLogFile == NULL || ver->versionLogFile == NULL || actCentral->transformTimeMutex == NULL ){
        return  0;
    }

    startTime = getDateTime(ver->startStamp, TIME, actCentral->transformTimeMutex);
    startDate = getDateTime(ver->startStamp, DATE, actCentral->transformTimeMutex);
    endTime = getDateTime(ver->endStamp, TIME, actCentral->transformTimeMutex);
    endDate = getDateTime(ver->endStamp, DATE, actCentral->transformTimeMutex);

    if (startTime == NULL || startDate == NULL || endTime == NULL  || endDate == NULL){
        inlineFree(4, &startTime,&startDate, &endTime, &endDate);
        return 0;
    }
    if (type == ACTION_LOG){
        resumeActionLog(ver, act, startTime, startDate, endTime, endDate);
    }
    else if (type == VERSION_LOG){
        resumeVersionLog(ver,endTime, endDate);
    }

    inlineFree(4, &startTime,&startDate, &endTime, &endDate);

    return 1;
}
