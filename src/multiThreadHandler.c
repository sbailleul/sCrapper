//
// Created by sbailleul on 10/20/19.
//
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <zconf.h>
#include <math.h>
#include <float.h>
#include <string.h>
#include <scrapper/fileWriter.h>
#include "scrapper/multiThreadHandler.h"
#include "scrapper/fileReader.h"
#include "scrapper/genericUtils.h"
#include "scrapper/actionThreadHandler.h"
#include "scrapper/scrapConfig.h"

char mainLoop(MultiThread *multiThread){
    int choice=0;
    sleep(2);

    do{
        printf("\nTo exit write 0 \nChoice : ...");
        if(scanf("%d",&choice) != EOF){
            createErrorReport("Can't read scanf input from user",__FILE__,__LINE__,__DATE__,__TIME__);
        }
    }while(choice);

    pthread_mutex_lock(&multiThread->endTaskThreadCntMutex);
    multiThread->runFlag =0;
    pthread_mutex_unlock(&multiThread->endTaskThreadCntMutex);

    pthread_cond_broadcast(&multiThread->endMultiThreadCond);

    return 1;
}

MultiThread *initMultiThread(ScrapConfig *scrapConfig, FileIndex *fileIndex){

    MultiThread *multiThread = NULL;

    int cntTaskThread = 0;
    if((multiThread=malloc(sizeof(MultiThread)))==NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }
    multiThread->taskThreadArr=NULL;
    multiThread->scrapConfig=scrapConfig;
    multiThread->taskThreadArrSize=getTaskThreadArrSize(scrapConfig->tasksArr, scrapConfig->tskArrSize);
    multiThread->actThreadsCentralArrSize=scrapConfig->actArrSize;
    multiThread->taskThreadArr = NULL;
    multiThread->actThreadsCentralArr = NULL;
    multiThread->runFlag=1;

    pthread_mutex_init(&multiThread->transformTimeMutex, NULL);
    pthread_mutex_init(&multiThread->checkMultiThreadEndMutex, NULL);
    pthread_mutex_init(&multiThread->endTaskThreadCntMutex, NULL);
    pthread_cond_init(&multiThread->endMultiThreadCond, NULL);


    if((multiThread->taskThreadArr=calloc(multiThread->taskThreadArrSize,sizeof(TaskThread*)))==NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        free(multiThread);
        return NULL;
    }
    if((multiThread->actThreadsCentralArr=calloc(multiThread->actThreadsCentralArrSize, sizeof(TaskThread*))) == NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        freeMultiThread((void**)&multiThread);
        return NULL;
    }
    for(int i=0 ; i<multiThread->actThreadsCentralArrSize; i++){
        if((multiThread->actThreadsCentralArr[i] = initActionsCentralizer(scrapConfig->actionsArr[i], scrapConfig->tasksArr, scrapConfig->tskArrSize, &multiThread->transformTimeMutex)) == NULL){
            freeMultiThread((void**)&multiThread);
            return NULL;
        }
    }
    for(int i=0; i<scrapConfig->tskArrSize; i++){
        int tskThreadArrSize = getTaskThreadArrSize(&scrapConfig->tasksArr[i],0);
        for(int instance=0 ; instance < tskThreadArrSize; instance++){
            if((multiThread->taskThreadArr[cntTaskThread]=initTaskThread(scrapConfig->tasksArr[i],multiThread,instance+1)) == NULL){
                freeMultiThread((void**)&multiThread);
                return NULL;
            }
            cntTaskThread++;
        }
    }
    multiThread->runningTaskCnt=multiThread->taskThreadArrSize;

    return multiThread;
}

char freeMultiThread(void **untypedMultiThread){

    MultiThread *multiThread = NULL;
    if(untypedMultiThread == NULL || *untypedMultiThread==NULL){
        return 0;
    }
    multiThread= (MultiThread*)*untypedMultiThread;

    freeStructArray((void***)&multiThread->actThreadsCentralArr, multiThread->actThreadsCentralArrSize, freeActionThreadCentralizer);
    freeStructArray((void***)&multiThread->taskThreadArr, multiThread->taskThreadArrSize, freeTaskThread);


    free(multiThread);
    multiThread=NULL;
    return 1;
}
ActionThreadCentralizer *initActionsCentralizer(Action *act, Task **tskArr, int tskArrSize, pthread_mutex_t* transformTimeMutex){

    ActionThreadCentralizer *actCentralizer = NULL;

    if(act == NULL){
        return NULL;
    }
    if((actCentralizer = malloc(sizeof(ActionThreadCentralizer))) == NULL){
        return NULL;
    }

    actCentralizer->action = act;
    actCentralizer->rootActTree = NULL;
    actCentralizer->actTreeBucket = NULL;
    actCentralizer->actTreeBucketSize = 0;
    actCentralizer->endedActThreadsCnt = 0;
    actCentralizer->actThreadArr=NULL;
    actCentralizer->actThreadArrSize=0;
    actCentralizer->runningActThreadCnt=0;
    actCentralizer->transformTimeMutex = transformTimeMutex;

    if (getOptionByKey(VERSIONING, act->optionsArr, act->optArrSize ) != NULL && !strcmp(getOptionByKey(VERSIONING, act->optionsArr, act->optArrSize )->value->string, "on")){
        actCentralizer->versionInfo = initVersionInfo(act, tskArr, tskArrSize);
    }

    pthread_mutex_init(&actCentralizer->transformVersionInfoMutex, NULL);
    pthread_mutex_init(&actCentralizer->transformActTreeMutex, NULL);
    pthread_mutex_init(&actCentralizer->transformBucketMutex, NULL);
    pthread_mutex_init(&actCentralizer->checkActThreadMutex, NULL);

    if(!initActionTreeBucketAndActionTreeRoot(act, &actCentralizer->rootActTree, &actCentralizer->actTreeBucket, &actCentralizer->actTreeBucketSize)){
        freeActionThreadCentralizer((void **) &actCentralizer);
        return NULL;
    }
    return actCentralizer;
}


char freeActionThreadCentralizer(void **untypedCentralizer){

    ActionThreadCentralizer *actCentralizer = NULL;

    if(untypedCentralizer == NULL || *untypedCentralizer == NULL){
        return 0;
    }
    actCentralizer = (ActionThreadCentralizer*) *untypedCentralizer;

    freeVersionInfo((void**)&actCentralizer->versionInfo);

    if(actCentralizer->rootActTree != NULL){
        freeActionTree((void**)&actCentralizer->rootActTree);
    }
    if(actCentralizer->actTreeBucket != NULL){
        free(actCentralizer->actTreeBucket);
        actCentralizer->actTreeBucketSize = 0;
    }

    return 1;
}

TaskThread *initTaskThread(Task *task, MultiThread *multiThread, int instanceID){

    TaskThread *taskThread = NULL;
    int actThreadCnt = 0;

    if((taskThread=malloc(sizeof(TaskThread)))==NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }
    taskThread->actionsThreadArrSize=task->actArrSize;

    if((taskThread->actionsThreadArr=malloc(sizeof(Action*)*taskThread->actionsThreadArrSize))==NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        free(taskThread);
        return NULL;
    }
    taskThread->multiThread=multiThread;
    taskThread->task=task;
    taskThread->runFlag=1;
    taskThread->instanceID= getOptionByKey(INSTANCE,task->optionsArr, task->optArrSize) != NULL ? instanceID : 0;
    taskThread->secFrequency = getOptionsTimeFrequency(task->optionsArr,task->optArrSize);
    taskThread->runningActThreadCnt = 0;

    pthread_cond_init(&taskThread->tskThreadRunIntervalCond, NULL);
    pthread_mutex_init(&taskThread->actionThreadRunMutex, NULL);
    pthread_mutex_init(&taskThread->checkTaskThreadMutex, NULL);

    for(int i=0 ; i < multiThread->actThreadsCentralArrSize ; i++){
        ActionThreadCentralizer *actThreadCentral = multiThread->actThreadsCentralArr[i];
        if(actThreadCentral != NULL && findPointerInPointerArray((void**)task->actionsArr,task->actArrSize,(void*)actThreadCentral->action) != NULL &&
            (taskThread->actionsThreadArr[actThreadCnt]=initActionThread(actThreadCentral, taskThread, multiThread)) != NULL) {
                pushElementInPointerArray((void ***) &actThreadCentral->actThreadArr,(unsigned long*)&actThreadCentral->actThreadArrSize, taskThread->actionsThreadArr[actThreadCnt],sizeof(ActionThread));
            actThreadCnt++;
        }
    }

    return taskThread;
}



int getTaskThreadArrSize(Task **taskArr, int tskArrSize){
    int tskThreadArrSize = 0;
    Option *opt = NULL;

    if(taskArr == NULL){
        return -1;
    }
    if(tskArrSize == 0){
        return  (opt = getOptionByKey(INSTANCE,(*taskArr)->optionsArr,(*taskArr)->optArrSize)) != NULL ? opt->value->integer : 1;
    }
    for(int i=0 ; i<tskArrSize ; i++){
        tskThreadArrSize += (opt = getOptionByKey(INSTANCE,taskArr[i]->optionsArr,taskArr[i]->optArrSize)) != NULL ? opt->value->integer : 1;
    }
    return tskThreadArrSize;
}

char freeTaskThread(void **untypedTaskThread){

    TaskThread *taskThread = NULL;
    if(untypedTaskThread!=NULL && *untypedTaskThread!=NULL){

        taskThread = (TaskThread*) *untypedTaskThread;
        freeStructArray((void ***) &taskThread->actionsThreadArr, taskThread->actionsThreadArrSize, freeActionThread);

        free(taskThread);
        taskThread=NULL;
        return 1;
    }
    return 0;
}

ActionThread *initActionThread(ActionThreadCentralizer *actCentralizer, TaskThread *taskThread, MultiThread *multiThread){

    ActionThread *actionThread = NULL;

    if((actionThread= malloc(sizeof(ActionThread)))==NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }

    actionThread->timed = getOptionsTimeFrequency(taskThread->task->optionsArr,taskThread->task->optArrSize)>0 ? 1 : 0;
    actionThread->taskThread=taskThread;
    actionThread->multiThread=multiThread;
    actionThread->actCentral=actCentralizer;
    actionThread->workFlag=1;

    return actionThread;
}

char freeActionThread(void **untypedActionThread){

    ActionThread *actionThread = NULL;
    if(untypedActionThread != NULL && *untypedActionThread!=NULL){
        actionThread = (ActionThread*) *untypedActionThread;
        free(actionThread);
        actionThread=NULL;
        return 1;
    }
    return 0;
}

void *launchMultiThread(void *untypedMultiThread){

    MultiThread *multiThread = NULL;
    int createThreadRes = 0;

    if(untypedMultiThread == NULL){
        return NULL;
    }
    multiThread = (MultiThread*) untypedMultiThread;

    for(int i=0;i<multiThread->taskThreadArrSize ;i++){
        if((createThreadRes = pthread_create(&multiThread->taskThreadArr[i]->tskThread,NULL,launchTaskThread,multiThread->taskThreadArr[i]))){
            createErrorReport("Error n° = %d creating task thread %s , instance %d :  ",__FILE__,__LINE__,__DATE__,__TIME__,
                    createThreadRes,multiThread->taskThreadArr[i]->task->name, multiThread->taskThreadArr[i]->instanceID);
        }
    }
    sleep(1);

    while(multiThread->runFlag){
        pthread_mutex_lock(&multiThread->checkMultiThreadEndMutex);
        pthread_cond_wait(&multiThread->endMultiThreadCond, &multiThread->checkMultiThreadEndMutex);
        pthread_mutex_unlock(&multiThread->checkMultiThreadEndMutex);

        pthread_mutex_lock(&multiThread->endTaskThreadCntMutex);
        if(multiThread->runFlag){
            multiThread->runFlag = multiThread->runningTaskCnt == 0 ? 0 : 1;
        }
        pthread_mutex_unlock(&multiThread->endTaskThreadCntMutex);
    }
    closeMultiThread(multiThread);

    for(int i=0;i<multiThread->taskThreadArrSize ;i++){
        pthread_join(multiThread->taskThreadArr[i]->tskThread,NULL);
    }

    return NULL;
}

char closeMultiThread(MultiThread *multiThread){

    TaskThread **taskThreadArr = NULL;
    int taskThreadArrSize;

    if(multiThread == NULL || (taskThreadArr = multiThread->taskThreadArr) == NULL){
        return 0;
    }
    multiThread->runFlag = 0;
    taskThreadArrSize = multiThread->taskThreadArrSize;

    for(int i=0 ; i < taskThreadArrSize ; i++){

        if(taskThreadArr[i] != NULL && taskThreadArr[i]->runFlag){
            taskThreadArr[i]->runFlag = 0;
            pthread_cond_broadcast(&taskThreadArr[i]->tskThreadRunIntervalCond);
        }
    }
}
void *launchTaskThread(void *untypedTaskThread){

    TaskThread *tskThread = untypedTaskThread;
    MultiThread *multiThread;
    int rs = 0;

    if(tskThread == NULL || (multiThread=tskThread->multiThread)== NULL){
        return NULL;
    }
    time(&tskThread->currentTime);
    time(&tskThread->lastTime);

    for(int i=0; i<tskThread->actionsThreadArrSize ; i++){
        if((rs = pthread_create(&tskThread->actionsThreadArr[i]->actThread, NULL, launchActionThread, tskThread->actionsThreadArr[i]))){
            createErrorReport("Error n° = %d creating action thread : %s in task thread %s instance %d:  ",__FILE__,__LINE__,__DATE__,__TIME__,
                    rs,tskThread->actionsThreadArr[i]->actCentral->action->name, tskThread->task->name, tskThread->instanceID );
        }
        tskThread->runningActThreadCnt++;
    }

    handleActionsThreadTiming(tskThread,&multiThread->runFlag);
    pthread_cond_broadcast(&tskThread->tskThreadRunIntervalCond);
    tskThread->runFlag=0;

    for(int i=0; i<tskThread->actionsThreadArrSize ; i++){
        pthread_join(tskThread->actionsThreadArr[i]->actThread, NULL);
    }

    pthread_mutex_lock(&multiThread->endTaskThreadCntMutex);
        multiThread->runningTaskCnt--;
    pthread_mutex_unlock(&multiThread->endTaskThreadCntMutex);

    pthread_cond_broadcast(&multiThread->endMultiThreadCond);
    printf("End task : %s , instance %d\n", tskThread->task->name, tskThread->instanceID);
    return NULL;
}

char handleActionsThreadTiming(TaskThread *tskThread, char *runFlag){

    while(tskThread->runFlag && *runFlag && tskThread->runningActThreadCnt){
        if(tskThread->secFrequency){
            time(&tskThread->currentTime);
            double diffTime = difftime(tskThread->currentTime,tskThread->lastTime);
            double diffFrequency = fabs(diffTime - tskThread->secFrequency);
            if(diffFrequency < DBL_EPSILON){
                pthread_cond_broadcast(&tskThread->tskThreadRunIntervalCond);
                tskThread->lastTime = tskThread->currentTime;
            }
        } else {
            pthread_mutex_lock(&tskThread->actionThreadRunMutex);
            pthread_cond_wait(&tskThread->tskThreadRunIntervalCond, &tskThread->actionThreadRunMutex);
            pthread_mutex_unlock(&tskThread->actionThreadRunMutex);
        }
    }
    return 1;
}

void *launchActionThread(void *untypedActionThread){

    ActionThread *actionTread = untypedActionThread;
    TaskThread *tskThread;
    ActionThreadCentralizer *actCentralizer;
    char endFlag = 1;

    if(actionTread == NULL || (tskThread = actionTread->taskThread) == NULL || (actCentralizer = actionTread->actCentral) == NULL){
        return NULL;
    }

    while(tskThread->runFlag && endFlag){
        if(actionTread->timed){
            pthread_mutex_lock(&tskThread->actionThreadRunMutex);
            pthread_cond_wait(&tskThread->tskThreadRunIntervalCond, &tskThread->actionThreadRunMutex);
        }
        ActionTree *tstTree = transformActionTreeBucket(&actCentralizer->actTreeBucket, NULL, &actCentralizer->actTreeBucketSize, &actCentralizer->transformBucketMutex);
        endFlag = checkActThreadRunning(actCentralizer, actionTread,tstTree != NULL);
        transformActionTree(actionTread ,tstTree);

        if(actionTread->timed) {
            pthread_mutex_unlock(&tskThread->actionThreadRunMutex);
        }
    }
    terminateActionToLogs(actCentralizer, tskThread->task);

    return NULL;
}

char terminateActionToLogs(ActionThreadCentralizer *actCentralizer, Task* tsk ){

    VersionInfo *versionInfo = NULL;

    if(actCentralizer == NULL || tsk == NULL ){
        return 0;
    }

    pthread_mutex_lock(&actCentralizer->transformVersionInfoMutex);
    if((versionInfo = actCentralizer->versionInfo)!=NULL) {
        actCentralizer->endedActThreadsCnt++;

        if (actCentralizer->endedActThreadsCnt == actCentralizer->actThreadArrSize) {

            time(&versionInfo->endStamp);
            if (resumeWriteLog(actCentralizer, ACTION_LOG) && resumeWriteLog(actCentralizer, VERSION_LOG)) {
            }
            printf("\nEnd of action : %s \n", actCentralizer->action->name);
        }
    }
    pthread_mutex_unlock(&actCentralizer->transformVersionInfoMutex);
    return 1;
}
