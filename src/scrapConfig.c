//
// Created by sbailleul on 10/13/19.
//
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <scrapper/genericUtils.h>
#include <time.h>

#include "scrapper/scrapConfig.h"
#include "scrapper/fileReader.h"
#include "scrapper/urlHandler.h"
#include "scrapper/stringUtils.h"
#include "scrapper/fileWriter.h"


int setActionsToTasks(Task ***tasksArr, Action **actionsArr, int tskArrSize, int actArrSize){

    int affectedActionsCnt=0;

    for(int i=0 ; i<tskArrSize ;i++){
        if((*tasksArr)[i]->optionsArr!=NULL){
            Task *tsk=(*tasksArr)[i];

            Option *opt = getOptionByKey(ACTIONS,tsk->optionsArr,tsk->optArrSize);
            if(opt!=NULL){
                int optActArrSize = 0;
                char **optActArr = splitStringOnSeparator(opt->value->string,&optActArrSize,",",ALL,DESTROY);
                if(optActArr != NULL){
                    for(int j=0 ; j < actArrSize ; j++){
                        Action *act=actionsArr[j];
                        if(findStringInStringArr(act->name,optActArr,optActArrSize)>=0){
                            if((tsk->actionsArr=realloc(tsk->actionsArr,sizeof(Action*)*(tsk->actArrSize+1)))==NULL){
                                recursFree((void***)&optActArr,&optActArrSize,1);
                                createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
                                return -1;
                            }
                            tsk->actionsArr[tsk->actArrSize]=act;
                            tsk->actArrSize++;
                            affectedActionsCnt++;
                        }
                    }
                    recursFree((void***)&optActArr,&optActArrSize,1);
                }
                freeOption((void**)&opt);
            }
        }
    }
    return affectedActionsCnt;
}

ScrapConfig *initScrapConfig(FILE *confFile, FILE *tagAttributeFile){

    ScrapConfig *scrapConf;

    if((scrapConf=malloc(sizeof(ScrapConfig)))==NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }
    scrapConf->actArrSize=0;
    scrapConf->tskArrSize=0;
    scrapConf->tagAttributeArrSize = 0;
    scrapConf->actionsArr=NULL;
    scrapConf->tasksArr=NULL;
    scrapConf->tagAttributeArr=NULL;


    readScrapConfig(confFile,scrapConf);
    scrapConf->tagAttributeArr=readTagAttributeList(tagAttributeFile,&scrapConf->tagAttributeArrSize);

    setActionsToTasks(&scrapConf->tasksArr,scrapConf->actionsArr,scrapConf->tskArrSize,scrapConf->actArrSize);

    return scrapConf;
}

char freeScrapConfig(void** untypedScrapConf){

    ScrapConfig **scrapConf = NULL;

    if(untypedScrapConf == NULL || *untypedScrapConf==NULL){
        return 0;
    }
    scrapConf = (ScrapConfig**)untypedScrapConf;
    freeStructArray((void ***) &(*scrapConf)->tasksArr, (*scrapConf)->tskArrSize, freeTask);
    freeStructArray((void ***) &(*scrapConf)->actionsArr, (*scrapConf)->actArrSize, freeAction);

    free(*scrapConf);
    *scrapConf=NULL;
    return 1;
}

Task *initTask(char ****keysValuesArr,Action **actArr, int *size, int actArrSize){

    Task *tsk =NULL;

    if((tsk=malloc(sizeof(Task)))==NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }
    tsk->actionsArr=actArr;
    tsk->optionsArr=NULL;

    if(cpyMapValuesByFormats(keysValuesArr, size, "%s=>name", &tsk->name)!=1){
        free(tsk);
        return NULL;
    }

    tsk->optArrSize=0;
    tsk->actArrSize=actArrSize;


    tsk->optionsArr=initOptionsArr(keysValuesArr,size,&tsk->optArrSize);

    return tsk;
}


char freeTask(void **untypedTsk){


    Task *tsk = NULL;

    if(untypedTsk == NULL || *untypedTsk==NULL){
        return 0;
    }
    tsk = (Task*)*untypedTsk;

    if(tsk->name !=NULL){
        free(tsk->name);
        tsk->name=NULL;
    }

    freeStructArray((void ***) &tsk->optionsArr, tsk->optArrSize, freeOption);


    if(tsk->actionsArr!=NULL){
        free(tsk->actionsArr);
        tsk->actionsArr=NULL;
    }

    free(tsk);
    tsk=NULL;
    return 1;
}



Action *initAction(char ****keysValuesArr, int *size){

    Action *act =NULL;
    char *escapedName = NULL;
    if((act=malloc(sizeof(Action)))==NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }
    act->name=NULL;
    act->url=NULL;
    act->directory = NULL;
    act->optArrSize=0;

    if(cpyMapValuesByFormats(keysValuesArr, size, "%s=>name %s=>url", &act->name, &act->url)!=2){
        free(act);
        return NULL;
    }
    if(cpyMapValuesByFormats(keysValuesArr, size, "%s=>directory", &act->directory)!=1){
        act->directory = strCpyAlloc("scrappingResult");
    } else {
        rtrim(&act->directory,"/",RIGHT);
    }

    if(act->url != NULL){
        formatUrl(&act->url);
    }

    strCatAlloc(&act->directory,"/",RIGHT);

    if((escapedName = strCpyAlloc(act->name)) != NULL){
        escapeStringToFileSystem(&escapedName);
        strCatAlloc(&act->directory,escapedName,RIGHT);
        free(escapedName);
    }

    act->optionsArr=initOptionsArr(keysValuesArr,size,&act->optArrSize);

    if(act->url != NULL){
        formatUrl(&act->url);
    }

    return act;
}

VersionInfo *initVersionInfo(Action* act, Task **tskArr, int tskArrSize){
    VersionInfo* ver =  NULL;
    char* dir= NULL;
    if ((ver=malloc(sizeof(VersionInfo))) == NULL)
    {
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }
    ver->startStamp = time(NULL);
    ver->endStamp = time(NULL);
    ver->totalFilesCount = 0;
    ver->countPerTypeArrSize = 0;
    ver->actionLogFile = NULL;
    ver->versionLogFile = NULL;
    ver->typeCounterArray = NULL;
    ver->typeKeysArray = NULL;
    ver->tskArr = NULL;
    ver->tskArrSize = 0;

    for(int i=0 ; i< tskArrSize ; i++){
        if(findPointerInPointerArray((void**) tskArr[i]->actionsArr, tskArr[i]->actArrSize, act) != NULL){
            Task *tsk =  tskArr[i];
            ver->tskArr = realloc(ver->tskArr, sizeof(Task*) * (ver->tskArrSize+1));
            ver->tskArr[ver->tskArrSize] = tskArr[i];
            ver->tskArrSize++;
        }
    }
    ver->actionLogFile = initActionLog(act);

    dir= calloc(50, sizeof(char));

    sprintf(dir, "/version_%ld[%s_%s]", ver->startStamp, __DATE__, __TIME__);
    act->directory = strCatAlloc(&act->directory, dir, RIGHT );
    free(dir);

    ver->versionLogFile = initVersionLog(act , ver);
    return ver;
}

char freeVersionInfo(void**untypedVersionInfo)
{
    VersionInfo *ver = NULL;

    if (untypedVersionInfo == NULL || *untypedVersionInfo == NULL) {
        return 0;
    }

    ver = (VersionInfo*) *untypedVersionInfo;

    recursFree((void ***)&ver->typeKeysArray, &ver->countPerTypeArrSize, 1);
    free(ver->typeCounterArray);
    fclose(ver->actionLogFile);
    fclose(ver->versionLogFile);
    if(ver->tskArr != NULL){
        free(ver->tskArr);
    }
    free(ver);

    ver = NULL;
    return 1;
}


char freeAction(void **untypedAct){

    Action *act = NULL;

    if(untypedAct == NULL || *untypedAct==NULL){
        return 0;
    }
    act = (Action*)*untypedAct;


    inlineFree(2,&act->name,&act->url);

    freeStructArray((void ***) &act->optionsArr, act->optArrSize, freeOption);
    act->optArrSize=0;


    free(act);
    act=NULL;
    return 1;
}

Option **initOptionsArr(char ****keysValuesArr, int *keysValuesArrSize, int *optArrSize){

    Option **optionsArr = NULL;
    Option *opt=NULL;

    if(*keysValuesArrSize>0){
        do{
            if((opt=initOption(keysValuesArr,keysValuesArrSize))!=NULL){
                if((optionsArr=realloc(optionsArr,sizeof(Option*)*(*optArrSize+1)))==NULL){
                    createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
                    return NULL;
                }
                optionsArr[*optArrSize]=opt;
                *optArrSize+=1;
            }

        }while(opt!=NULL);
    }
    return optionsArr;
}


Option *initOption(char ****keysValuesArr, int *keysValuesArrSize){

    Option *opt =NULL;
    char formatList[KEYS_ARR_SIZE][FORMAT_SIZE]={
            {"%s=>versioning"},
            {"%d=>max-depth"},
            {"%s=>type"},
            {"%d=>second"},
            {"%d=>minute"},
            {"%d=>hour"},
            {"%d=>day"},
            {"%s=>actions"},
            {"%d=>instance"}
    };
    Key keys[KEYS_ARR_SIZE]={VERSIONING,MAX_DEPTH,CONTENT_TYPES, SECONDS,MINUTES,HOURS,DAYS,ACTIONS,INSTANCE};

    if((opt=malloc(sizeof(Option)))==NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }

    for(int i=0 ; i<KEYS_ARR_SIZE ; i++){
        if((opt->value=initValue(keysValuesArr, keysValuesArrSize, formatList[i])) != NULL){
            opt->key=keys[i];
            return opt;
        }
    }
    free(opt);
    return NULL;
}

char freeOption(void **untypedOpt){

    Option *opt = NULL;

    Key keys[8]={MAX_DEPTH,VERSIONING,CONTENT_TYPES, SECONDS,MINUTES,HOURS,DAYS,ACTIONS};
    Type types[8]={INTEGER,STRING,STRING,INTEGER,INTEGER,INTEGER,INTEGER,STRING};
    if( untypedOpt!=NULL && *untypedOpt!=NULL){
        opt = (Option*) *untypedOpt;

        if(opt->value!=NULL){
            for(int i=0; i<8; i++){
                if(opt->key==keys[i]){
                    freeValue(&opt->value,types[i]);
                }
            }
        }
        free(opt);
        opt=NULL;
        return 1;
    }
    return 0;
}

Value *initValue(char ****keysValuesArr, int *keysValuesArrSize, char *formatKey){

    Value *val;

    if((val=malloc(sizeof(Value)))==NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }
    if(strncmp(formatKey,"%c",2)==0 && cpyMapValuesByFormats(keysValuesArr, keysValuesArrSize, formatKey, &val->character)){
        return val;
    }else if(strncmp(formatKey,"%s",2)==0 && cpyMapValuesByFormats(keysValuesArr, keysValuesArrSize, formatKey, &val->string)){
        return val;
    } else if((strncmp(formatKey,"%d",2)==0||strncmp(formatKey,"%ld",3)==0 ) && cpyMapValuesByFormats(keysValuesArr, keysValuesArrSize, formatKey, &val->integer)){
        return val;
    }else if((strncmp(formatKey,"%f",2)==0||strncmp(formatKey,"%lf",3)==0) && cpyMapValuesByFormats(keysValuesArr, keysValuesArrSize, formatKey, &val->real)){
        return val;
    }
    free(val);
    return NULL;
}

char freeValue(Value ** val,Type type){

    if(*val!=NULL){
        if(type==STRING && (*val)->string!=NULL){
            free((*val)->string);
            (*val)->string=NULL;
        }
        free(*val);
        *val=NULL;
        return 1;
    }
    return 0;
}

unsigned long getOptionsTimeFrequency(Option **optionArr, int optionArrSize ){

    Key timeKeys[TIME_KEYS_ARR_SIZE]={SECONDS,MINUTES,HOURS,DAYS};
    int factorArr[TIME_KEYS_ARR_SIZE]={1,60,3600,86400};
    unsigned long secondFrequency=0;

    for(int i=0 ; i<optionArrSize ; i++){

        for(int j=0 ; j<TIME_KEYS_ARR_SIZE ;j++){
            if(optionArr[i]->key==timeKeys[j]){
                secondFrequency+=(optionArr[i]->value->integer*factorArr[j]);
            }
        }
    }

    return secondFrequency;
}

Option *getOptionByKey(Key key, Option **optionArr, int optionArrSize){
    if (optionArr == NULL)
    {
        return NULL;
    }
    for(int i=0; i<optionArrSize ; i++){
        if(optionArr[i]->key == key){
            return optionArr[i];
        }
    }
    return NULL;
}

TagAttribute *initTagAttribute(char *tagName, char *attributeName){

    TagAttribute *tagAttribute = NULL ;

    if(tagName == NULL || attributeName == NULL){
        return NULL;
    } else if( (tagAttribute = malloc(sizeof(TagAttribute))) == NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }

    if((tagAttribute->tagName = strCpyAlloc(tagName)) == NULL || (tagAttribute->attributeName = strCpyAlloc(attributeName)) == NULL){
        freeTagAttribute((void**)&tagAttribute);
    }
    return tagAttribute;
}

char freeTagAttribute(void **untypedTagAttribute){

    TagAttribute *tagAttribute = NULL;

    if(untypedTagAttribute == NULL || *untypedTagAttribute == NULL){
        return 0;
    }
    tagAttribute = (TagAttribute*)*untypedTagAttribute;

    if(tagAttribute->tagName != NULL){
        free(tagAttribute->tagName);
    }
    if(tagAttribute->attributeName != NULL){
        free(tagAttribute->attributeName);
    }
    return 1;
}


