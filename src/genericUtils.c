//
// Created by sbailleul on 10/28/19.
//
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <scrapper/fileHandler.h>
#include <time.h>
#include <pthread.h>

#include "scrapper/genericUtils.h"
#include "scrapper/stringUtils.h"

int mergeArrays(void ***dstArr, int dstArrSize,  void *** addedArr, int addedArrSize, size_t elementsSize){

    int newSize = dstArrSize + addedArrSize;

    if(addedArr == NULL || *addedArr == NULL){
        return dstArrSize;
    }
    if((*dstArr=realloc(*dstArr,elementsSize * (newSize)))==NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return 0;
    }
    for(int i=dstArrSize ; i <newSize ; i++){
        (*dstArr)[i]=(*addedArr)[i-dstArrSize];
    }
    return newSize;
}


int cpyMapValuesByFormats(char ****keysValuesArr, int *sizeArrVal, const char * formats , ...){

    int formatSize=0, tmpSize=*sizeArrVal;
    char **formatArr ;
    int arrSizeRecurse = 2;

    va_list params;
    va_start(params, formats);

    if((formatArr=getFormatArr(formats,&formatSize))==NULL){
        return 0;
    }

    for(int i=0 ; i<tmpSize  ; i++){
        va_start(params,formats);
        char * srcKey=(*keysValuesArr)[i][0];
        char foundFlag=0;

        for(int j=0 ;j<formatSize ;j++){
            void * pV= va_arg(params,void*);
            if(formatArr[j]!=NULL){ // string : %format key=>value
                char ** subFormArr=splitStringOnSeparator(formatArr[j],NULL,"=>",ALL,DESTROY);
                if(subFormArr!=NULL){
                    if(strncmp(subFormArr[1],srcKey,strlen(srcKey))==0){
                        foundFlag=1;

                        if(affectTypedValToVoidPointerByFormat((*keysValuesArr)[i][1],pV,subFormArr[0])){
                            free(formatArr[j]);
                            formatArr[j]=NULL;
                        }
                    }
                    recursFree((void***)&subFormArr,&arrSizeRecurse,1);
                }
            }
        }
        if(foundFlag){
            recursFree((void***)&(*keysValuesArr)[i],&arrSizeRecurse,1);
            *sizeArrVal-=1;
        }
    }
    if(tmpSize!=*sizeArrVal){
        if(*sizeArrVal == 0){
            inlineFree(1,keysValuesArr);
        } else {
            reduceToStartHoledPntArr((void**)(*keysValuesArr),tmpSize);
            if((*keysValuesArr=realloc(*keysValuesArr, sizeof(char **) * (*sizeArrVal))) == NULL){
                createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
                return -1;
            }
        }
    }
    for(int j=0;j<formatSize;j++){
        if(formatArr[j]!=NULL){
            free(formatArr[j]);
        }
    }
    free(formatArr);

    return tmpSize-*sizeArrVal;
}

int inlineFree(int ptCounter,...){

    int freedCounter=0;
    va_list params;
    va_start(params, ptCounter);

    for(int i=0 ; i<ptCounter; i++){
        void ** pV= va_arg(params,void*);
        if(pV!=NULL){
            free(*pV);
            *pV=NULL;
            freedCounter++;
        }
    }
    return freedCounter;
}


int affectTypedValToVoidPointerByFormat(char *srcVal, void *dstVal, char *format){

    if(format[0] == 's'){
        cpyStringToVoid(dstVal, srcVal);
        return 1;
    } else if(format[0] == 'c'){
        char *cVal = (char*)dstVal;
        *cVal = srcVal[0];
        return 1;
    } else if(format[0] == 'd' || strncmp(format,"ld",2)==0){
        long *lVal = (long*)dstVal;
        *lVal = atol(srcVal);
        return 1;
    }  else if(format[0] == 'f' || strncmp(format,"lf",2)==0){
        double *dVal = (double*)dstVal;
        *dVal = atof(srcVal);
        return 1;
    }
    return 0;
}


int reduceToStartHoledPntArr(void **pntArr, int size){

    int nonNullPntCnt=0;

    for(int i=0 ; i<size; i++){
        char shiftFlag=0;
        for(int j=i+1; j<size ;j++){
            if(pntArr[i] == NULL && pntArr[j] != NULL){
                shiftFlag=1;
                void* tmpPnt = pntArr[i];
                pntArr[i]=pntArr[j];
                pntArr[j]=tmpPnt;
            }
        }
        if(shiftFlag){
            nonNullPntCnt++;
        }
    }
    return nonNullPntCnt;
}


int recursFree(void ***pntArr, int *sizeArr, int i){

    int freeCount=1;

    if(pntArr!=NULL && *pntArr!=NULL){
        if(i>=1){
            for(int j=sizeArr[i-1]-1;j>=0;j--){
                freeCount+=recursFree((void ***)&(*pntArr)[j],sizeArr,i-1);
            }
        }
        free(*pntArr);
        *pntArr=NULL;
    }
    return freeCount;
}

char cpyStringToVoid(void ** vDstVal, char *srcVal){

    char ** dstVal= (char**)vDstVal;

    if((*dstVal=malloc(sizeof(char)*(strlen(srcVal)+1)))==NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return 0;
    }
    strcpy(*dstVal,srcVal);
    return 1;
}

int invertArrOrder(void **arr, int start, int end){

    int j = 1;
    int interval = end-start;
    int limit = interval/2;
    limit = interval % 2 == 0 ? limit : limit +1 ;

    for(int i=start ; i<limit || interval==2; i++){
        void * tmpVal=arr[i];
        arr[i] = arr[end-j];
        arr[end-j]=tmpVal;
        j++;
        interval = 0;
    }
    return j;
}

char freeStructArray(void ***arr, int arrSize, char freeFunction(void**)){

    if(arr != NULL && *arr != NULL){
        for(int i = 0; i<arrSize ; i++){

            if((*arr)[i]!=NULL){
                freeFunction(&(*arr)[i]);
            }
        }
        free(*arr);
        *arr=NULL;
    }
    return 1;
}

void *popElementFromPointerArray(void ***arr, unsigned long *arrSize, size_t typeSize){

    void *poppedElement = NULL;

    if(arr == NULL || *arr==NULL || *arrSize == 0){
        return NULL;
    }

    if((poppedElement = (*arr)[*arrSize-1]) == NULL){
        return NULL;
    }

    (*arr)[(*arrSize)-1] = NULL;
    if((*arr = realloc(*arr,sizeof(typeSize) * ((*arrSize)-1)))==NULL && *arrSize-1 ){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }
    *arrSize -= 1;

    return poppedElement;
}

char pushElementInPointerArray(void ***arr, unsigned long *arrSize, void *pushedElement, size_t typeSize){


    if(arr == NULL || pushedElement == NULL){
        return 0;
    }
    if((*arr = realloc(*arr,sizeof(typeSize) * (*arrSize+1)))==NULL && *arrSize+1){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return -1;
    }
    (*arr)[*arrSize] = pushedElement;

    *arrSize += 1;

    return 1;
}

void *findPointerInPointerArray(void ** ptArr, int ptArrSize, void *searchedPt){

    if(ptArr == NULL || searchedPt == NULL){
        return NULL;
    }

    for(int i=0 ; i<ptArrSize ; i++){
        if(ptArr[i] == searchedPt){
            return searchedPt;
        }
    }
    return NULL;

}

