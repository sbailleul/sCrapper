//
// Created by sbailleul on 10/22/19.
//

#include <stdlib.h>
#include <string.h>
#include "scrapper/fileHandler.h"
#include "scrapper/actionTree.h"
#include "scrapper/urlHandler.h"
#include "scrapper/stringUtils.h"
#include "scrapper/genericUtils.h"
#include "scrapper/fileWriter.h"


ActionTree *initActionTree(UrlSection *urlSection, ActionTree *parentTree, ActionTree **actTreeArr, unsigned long actTreeArrSize){

    ActionTree *actionTree = NULL;

    if((actionTree = malloc(sizeof(ActionTree)))==NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }

    actionTree->parentTree = parentTree;
    actionTree->urlSection = NULL;

    if(actTreeArr==NULL && actTreeArrSize){
        if((actionTree->actTreeArr = malloc(sizeof(ActionTree))) == NULL){
            createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
            return NULL;
        }
    } else {
        actionTree->actTreeArr = actTreeArr;
    }
    if(urlSection != NULL){
        if((actionTree->urlSection = initUrlSection(urlSection->value, urlSection->type)) == NULL){
            freeActionTree((void**)&actionTree);
        }
    }
    actionTree->actTreeArrSize=actTreeArrSize;

    return actionTree;
}

char freeActionTree(void **untypedActTree){

    ActionTree *actTree=NULL;
    if(untypedActTree == NULL || *untypedActTree == NULL){
        return 0;
    }
    actTree = (ActionTree*)*untypedActTree;

    freeStructArray((void ***) &actTree->actTreeArr, actTree->actTreeArrSize, freeActionTree);

    if(actTree->urlSection!=NULL){
        freeUrlSection((void**)&actTree->urlSection);
    }

    return 1;
}


ActionTree* setActionTreeByUrl(ActionTree *parentActTree, ActionTree **lastTree, UrlSection **urlSecArr,  int urlSecArrSize, int maxDepth, int iteration){

    ActionTree *actTree = NULL;
    int deepness=0;
    int actTreeArrSize=0;


    if(iteration >= urlSecArrSize){
        return NULL;
    }
    deepness=getDeepnessFromUrlSectionsArr(urlSecArr, iteration);
    if((maxDepth != -1 && deepness < maxDepth) || urlSecArr[iteration]->type != _PATH_ || maxDepth == -1 ){


        if(maxDepth != -1 && deepness == maxDepth && urlSecArr[iteration]->type != _PATH_ && (iteration + 1 == urlSecArrSize || urlSecArr[iteration + 1]->type == _PATH_)){//Don't create ActionTree arr if deepness == maxDepth and current url type == _DOMAIN_
            actTreeArrSize =  0;
        } else {
            actTreeArrSize = iteration < urlSecArrSize - 1 && getDeepnessFromUrlSectionsArr(urlSecArr,iteration+1) != maxDepth ? 1 : 0;
        }
        if(parentActTree != NULL){
            actTree = initActionTree(urlSecArr[iteration], parentActTree, NULL, actTreeArrSize);
        }
        else{
            actTree = initActionTree(NULL, NULL, NULL, actTreeArrSize);
            iteration--;
        }
        if(actTree != NULL && actTreeArrSize){
            actTree->actTreeArr[0] = setActionTreeByUrl(actTree, lastTree, urlSecArr, urlSecArrSize, maxDepth, ++iteration);
        }
        if(actTreeArrSize == 0){
            *lastTree = actTree;
        }
    }

    return actTree;
}


UrlSection  ** getUrlSectionArrFromActionTree(ActionTree *actTree, int *urlSecArrSize){

    ActionTree *tmpActTree = actTree;
    UrlSection **urlSectionArr = NULL;

    if(urlSecArrSize==NULL){
        return NULL;
    }
    *urlSecArrSize = 0;
    while(tmpActTree!=NULL) {
        if(tmpActTree->urlSection != NULL){
            if((urlSectionArr = realloc(urlSectionArr , sizeof(UrlSection*) * (*urlSecArrSize+1)))== NULL){
                createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
                return NULL;
            }
            urlSectionArr[*urlSecArrSize] = tmpActTree->urlSection;
            *urlSecArrSize+=1;
        }
        tmpActTree = tmpActTree->parentTree;
    }

    invertArrOrder((void**)urlSectionArr,0,*urlSecArrSize);

    invertDomainOrderInUrlSectionArr(urlSectionArr,*urlSecArrSize);


    return urlSectionArr;
}

ActionTree *findDeepestActionTreeByUrl(ActionTree *actTree, UrlSection **urlSectionArr, int urlSectionArrSize, int *iteration){

    int rootFlag = actTree != NULL && actTree->urlSection==NULL && *iteration==0;

    if(actTree == NULL || urlSectionArr==NULL || *iteration == urlSectionArrSize || (actTree->urlSection==NULL && !rootFlag) || urlSectionArr[*iteration]==NULL){
        return NULL;
    }

    if( !rootFlag && strcmp(urlSectionArr[*iteration]->value, actTree->urlSection->value) != 0 ){
        return NULL;
    }
    *iteration += 1 - rootFlag;
    ActionTree *foundActTree = actTree;

    if(actTree->actTreeArr!=NULL){
        for(unsigned long i=0 ; i < actTree->actTreeArrSize ; i++){
            if((foundActTree = findDeepestActionTreeByUrl(actTree->actTreeArr[i], urlSectionArr, urlSectionArrSize ,iteration)) != NULL){
                break;
            }
        }
    }
    if(foundActTree == NULL){
        return actTree;
    }
    return foundActTree;

}

FILE *writeActionTreeToFileSystem(ActionTree *actTree,char *workingDir){


    FILE *createdFile = NULL;
    char *path = NULL;

    if(actTree == NULL || workingDir == NULL){
        return NULL;
    }
    path = getResourceLocatorByActionTree(actTree, workingDir);
    createdFile = createFileDir(path);
    free(path);

    return createdFile;
}

char *getResourceLocatorByActionTree(ActionTree *actTree, char *workingDir){

    int urlSecArrSize = 0;
    UrlSection ** urlSecArr = NULL;
    char *resourceLocator = NULL;
    int endDomainIndex = -1;

    if((urlSecArr = getUrlSectionArrFromActionTree(actTree,&urlSecArrSize))==NULL){
        return NULL;
    }

    if(workingDir != NULL){
        if(findUrlSectionByType(urlSecArr, urlSecArrSize, 0, &endDomainIndex, _DOMAIN_) != NULL){
            invertArrOrder((void**)urlSecArr,1,endDomainIndex+1);
        }
        resourceLocator = convertUrlSectionArrToResourceLocator(urlSecArr, 0, urlSecArrSize, _FS_PATH_);
        strCatAlloc(&resourceLocator, "/", LEFT);
        strCatAlloc(&resourceLocator, workingDir, LEFT);
    } else {
        resourceLocator = convertUrlSectionArrToResourceLocator(urlSecArr, 0, urlSecArrSize, _URL_);
    }

    free(urlSecArr);

    return resourceLocator;
}

ActionTree *addActionTreeByAbsoluteUrl(ActionTree *startActTree,char *currentUrl, char *absoluteUrl, int maxDepth){

    ActionTree *lastActTree = NULL;
    ActionTree *newActTree = NULL;
    UrlSection **urlSectionArr = NULL;
    char **stringArr = NULL;
    unsigned int stringArrSizeToControl = 0;
    int urlSecSize = 0;
    int iteration =0;

    if(!compareAbsoluteUrls(currentUrl,absoluteUrl)){
        return NULL;
    }

    if((urlSectionArr = getUrlSectionsFromAbsoluteUrl(absoluteUrl,&urlSecSize))==NULL){
        return NULL;
    }
    if((stringArr = convertUrlSectionArrToStringArr(urlSectionArr,urlSecSize))==NULL){
        return NULL;
    }

    stringArrSizeToControl = getStringArrSize(stringArr,0,urlSecSize);

    invertDomainOrderInUrlSectionArr(urlSectionArr, urlSecSize);

    if((startActTree = findDeepestActionTreeByUrl(startActTree,urlSectionArr,urlSecSize,&iteration)) == NULL){
        return NULL;
    }
    if((newActTree = setActionTreeByUrl(startActTree, &lastActTree, urlSectionArr, urlSecSize, maxDepth, iteration)) == NULL){
        return NULL;
    }
    if((startActTree->actTreeArr = realloc(startActTree->actTreeArr, sizeof(ActionTree*) * (startActTree->actTreeArrSize + 1))) == NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }
    startActTree->actTreeArr[startActTree->actTreeArrSize] = newActTree;
    startActTree->actTreeArrSize++;

    freeStructArray((void ***) &urlSectionArr, urlSecSize, freeUrlSection);
    free(stringArr);

    if(stringArrSizeToControl>=PATH_MAX){
        return NULL;
    }
    return lastActTree;
}


char  initActionTreeBucketAndActionTreeRoot(Action *act, ActionTree **rootActTree, ActionTree ***actTreeBucket, unsigned long *actTreeBucketSize){

    Option *opt = NULL;
    UrlSection **urlSectionArr = NULL;
    ActionTree *lastActTree = NULL;
    int maxDepth = -1;
    int urlSecSize=0;

    if(act == NULL || rootActTree == NULL || actTreeBucket == NULL || actTreeBucketSize == NULL) {
        return 0;
    }

    act->url=rtrim(&act->url, "?", ALL);
    opt = getOptionByKey(MAX_DEPTH, act->optionsArr, act->optArrSize);
    maxDepth = opt != NULL ? opt->value->integer : -1 ;

    if((urlSectionArr = getUrlSectionsFromAbsoluteUrl(act->url, &urlSecSize)) == NULL ){
        return 0;
    }
    invertDomainOrderInUrlSectionArr(urlSectionArr, urlSecSize);

    *rootActTree = setActionTreeByUrl(NULL, &lastActTree, urlSectionArr, urlSecSize, maxDepth, 0);

    if(*rootActTree == NULL || lastActTree == NULL){
        return 0;
    }

    if((*actTreeBucket = malloc(sizeof(ActionTree *))) == NULL ){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return 0;
    }
    (*actTreeBucket)[0] = lastActTree;
    *actTreeBucketSize = 1;

    return 1;
}
