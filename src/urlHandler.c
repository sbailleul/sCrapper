//
// Created by sbailleul on 10/28/19.
//

#include <string.h>
#include <stdlib.h>
#include <curl/curl.h>
#include <scrapper/genericUtils.h>
#include <scrapper/fileHandler.h>
#include "scrapper/actionTree.h"
#include "scrapper/urlHandler.h"
#include "scrapper/stringUtils.h"



UrlSection* initUrlSection(char *value, UrlType urlType){

    UrlSection *urlSec = NULL;
    if(value == NULL){
        return NULL;
    }
    if((urlSec=malloc(sizeof(UrlSection)))==NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }
    urlSec->value=NULL;

    if((urlSec->value=strCpyAlloc(value)) == NULL){
        freeUrlSection((void**)&urlSec);
        return NULL;
    }
    urlSec->type = urlType;

    return urlSec;

}

char freeUrlSection(void **untypedUrlSec){

    UrlSection *urlSec = NULL;

    if(untypedUrlSec==NULL || *untypedUrlSec==NULL){
        return 0;
    }

    urlSec = (UrlSection*) *untypedUrlSec;

    if(urlSec->value != NULL){
        free(urlSec->value);
        urlSec->value=NULL;
    }

    free(urlSec);
    urlSec=NULL;
    return 1;
}

char formatUrl(char **srcUrl) {

    int splitArrSize=0;
    char **splitArr=NULL;
    UrlSection *tmpSec;

    if(srcUrl==NULL || *srcUrl==NULL){
        return 0;
    }
    if((tmpSec = getUnsecablePartFromAbsoluteUrl(*srcUrl, _PROTOCOL_)) == NULL){
        strCatAlloc(srcUrl, "https://",LEFT);
    }else {
        freeUrlSection((void**)&tmpSec);
    }
    if((tmpSec = getUnsecablePartFromAbsoluteUrl(*srcUrl, _QUERY_)) == NULL){
        if((splitArr=splitStringOnSeparator(*srcUrl, &splitArrSize, "/", RIGHT, KEEP)) == NULL){
            return 0;
        }

        free(*srcUrl);
        *srcUrl = NULL;
        strMergeFromArr(splitArr, 0,splitArrSize, srcUrl);
        recursFree((void***)&splitArr, &splitArrSize, 1);

    } else {
        freeUrlSection((void**)&tmpSec);
    }

    return 1;
}



UrlSection **getUrlSectionsFromAbsoluteUrl(char *url, int *urlSecArrSize){

    UrlSection **urlSecArr=NULL;
    UrlSection **tmpUrlSecArr=NULL;
    UrlSection *tmpSec;
    int tmpArrSize=0;
    int newSize=0;

    rtrim(&url,"/",RIGHT);
    if((tmpSec = getUnsecablePartFromAbsoluteUrl(url, _PROTOCOL_)) == NULL){
        return NULL;
    }
    if((urlSecArr=malloc(sizeof(UrlSection*)))==NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        freeUrlSection((void**)&tmpSec);
        return NULL;
    }
    urlSecArr[0] = tmpSec;

    if((tmpUrlSecArr= getDomainsFromAbsoluteUrl(url, &tmpArrSize)) == NULL){
        freeStructArray((void ***) &urlSecArr, 1, freeUrlSection);
        return NULL;
    }
    newSize= mergeArrays((void***)&urlSecArr,1,(void***)&tmpUrlSecArr,tmpArrSize,sizeof(UrlSection*));
    free(tmpUrlSecArr);


    tmpUrlSecArr = getPathsFromAbsoluteUrl(url, &tmpArrSize);
    newSize= mergeArrays((void***)&urlSecArr,newSize,(void***)&tmpUrlSecArr,tmpArrSize,sizeof(UrlSection*));
    free(tmpUrlSecArr);

    if((tmpSec = getUnsecablePartFromAbsoluteUrl(url, _QUERY_)) != NULL){
        if((urlSecArr=realloc(urlSecArr,sizeof(UrlSection*)*(newSize + 1)))==NULL){
            createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
            freeUrlSection((void**)&tmpSec);
            return NULL;
        }
        urlSecArr[newSize]=tmpSec;
        newSize++;
    }

    *urlSecArrSize = newSize;
    return urlSecArr;
}

UrlSection *getUnsecablePartFromAbsoluteUrl(char *url, UrlType urlType){

    UrlSection *urlSection=NULL;
    char *protocol;

    CURLUPart urlPart = urlType == _PROTOCOL_ ? CURLUPART_SCHEME : CURLUPART_QUERY;

    if((protocol=getUrlPartFromAbsoluteUrl(url,urlPart))==NULL){
        return NULL;
    }
    if((urlSection=initUrlSection(protocol,urlType))==NULL){
        free(protocol);
        return NULL;
    }
    free(protocol);
    return urlSection;
}

UrlSection **getDomainsFromAbsoluteUrl(char *url, int *domainSectionsArrSize) {

    UrlSection **urlSectionArr=NULL;
    char **domainsArr;
    char *domains =NULL;
    int domainTypeCnt=2;
    UrlType urlTypeArr[DOMAIN_TYPES_ARR_SIZE]={_DOMAIN_, _SECONDARY_DOMAIN_, _SUB_DOMAIN_};

    *domainSectionsArrSize=0;

    if((domains=getUrlPartFromAbsoluteUrl(url,CURLUPART_HOST))==NULL){
        return NULL;
    }
    if((domainsArr=splitStringOnSeparator(domains,domainSectionsArrSize,".",ALL,DESTROY))!=NULL && *domainSectionsArrSize>=2){

        if((urlSectionArr = malloc(sizeof(UrlSection*)*(*domainSectionsArrSize)))!=NULL) {
            domainTypeCnt = *domainSectionsArrSize == 3 ? 2 : 1;
            for (int i = 0; i < *domainSectionsArrSize; i++) {
                if ((urlSectionArr[i] = initUrlSection(domainsArr[i], urlTypeArr[domainTypeCnt])) == NULL) {
                    freeStructArray((void ***) &urlSectionArr, i, freeUrlSection);
                    break;
                }
                domainTypeCnt--;
            }
        } else {
            createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        }
        recursFree((void***)&domainsArr,domainSectionsArrSize,1);
    }
    free(domains);

    return urlSectionArr;
}

UrlSection **getPathsFromAbsoluteUrl(char *url, int *pathSectionsArrSize){

    UrlSection **urlSectionArr=NULL;
    char *urlPath= NULL;
    char **pathsArr;
    *pathSectionsArrSize=0;

    if((urlPath=getUrlPartFromAbsoluteUrl(url,CURLUPART_PATH))==NULL){
        return NULL;
    }
    if((pathsArr=splitStringOnSeparator(urlPath, pathSectionsArrSize, "/", ALL ,DESTROY)) == NULL){
        recursFree((void***)&pathsArr, pathSectionsArrSize, 1);
        return NULL;
    }
    if((urlSectionArr=malloc(sizeof(UrlSection*)*(*pathSectionsArrSize)))!=NULL){
        for(int i=0 ; i < *pathSectionsArrSize ; i++) {
            if((urlSectionArr[i]=initUrlSection(pathsArr[i], _PATH_)) == NULL){
                freeStructArray((void ***) &urlSectionArr, i, freeUrlSection);
                break;
            }
        }
    } else {
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
    }
    free(urlPath);
    recursFree((void***)&pathsArr, pathSectionsArrSize, 1);
    return urlSectionArr;
}

char *getUrlPartFromAbsoluteUrl(char *url,CURLUPart what){
    char *urlPart=NULL;
    char *urlPartCpy=NULL;
    CURLUcode rc;

    CURLU *curlUrl = NULL;

    if((curlUrl = curl_url())==NULL){
        return NULL;
    }
    rc = curl_url_set(curlUrl, CURLUPART_URL, url, 0);
    if(!rc) {
        rc = curl_url_get(curlUrl, what, &urlPart, 0);
        if(!rc) {
            if(strlen(urlPart)>1){
                urlPartCpy= strCpyAlloc(urlPart);
            }
        }
        curl_url_cleanup(curlUrl);
    }
    if(urlPart != NULL){
        curl_free(urlPart);
    }
    return urlPartCpy;
}

char *getAbsoluteUrlFromRelativeUrl(CURLU *curlUrl , char *srcUrl,  char *relativeUrl){
    char *absoluteUrl=NULL;
    char *absoluteUrlCpy=NULL;
    CURLUcode rc = 0;
    char curlUrlNullFlag = 0;

    if(curlUrl == NULL){
        curlUrlNullFlag=1;
        if(srcUrl == NULL){
            return NULL;
        }else {
            curlUrl = curl_url();
            rc = curl_url_set(curlUrl, CURLUPART_URL, srcUrl, 0);
        }
    }
    if(!rc){
        rc = curl_url_set(curlUrl, CURLUPART_URL, relativeUrl, 0);

    }
    if(!rc) {
        rc = curl_url_get(curlUrl, CURLUPART_URL, &absoluteUrl, 0);
        if(!rc) {
            absoluteUrlCpy= strCpyAlloc(absoluteUrl);
        }
        if(curlUrlNullFlag){
            curl_url_cleanup(curlUrl);
        }
    }
    if(absoluteUrl != NULL){
        curl_free(absoluteUrl);
    }
    return absoluteUrlCpy;
}

int getDeepnessFromUrlSectionsArr(UrlSection **urlSecArr, int urlSecArrSize) {

    int deepnessCnt =0;
    UrlType urlTypeArr[DEEPNESS_ARR_SIZE] = {_PATH_, _SUB_DOMAIN_};

    for(int i=0 ; i<urlSecArrSize ;i++){
        for(int j=0 ; j < DEEPNESS_ARR_SIZE ; j++){
            UrlSection *urlsecArrElement = urlSecArr[i];
            if(urlSecArr[i]->type == urlTypeArr[j]){
                deepnessCnt ++;
            }
        }
    }
    return deepnessCnt;
}

int invertDomainOrderInUrlSectionArr(UrlSection **urlSecArr, int urlSecArrSize ){

    int domainStart = -1;
    int domainsEnd = 0;
    UrlType domainsType[DOMAIN_TYPES_ARR_SIZE] = {_SUB_DOMAIN_, _SECONDARY_DOMAIN_, _DOMAIN_};

    if(urlSecArr==NULL || *urlSecArr == NULL){
        return -1;
    }

    for(int i=0 ; i<urlSecArrSize ; i++){

        for(int j =0 ; j<DOMAIN_TYPES_ARR_SIZE ; j++) {

            if(urlSecArr[i]->type == domainsType[j]){
                if(domainStart == -1){
                    domainStart=i;
                    domainsEnd=i;
                } else {
                    domainsEnd++;
                }
                break;
            }
        }
    }

    invertArrOrder((void**)urlSecArr, domainStart, domainsEnd+1);

    return domainStart-domainsEnd;
}

UrlSection *findUrlSectionByType(UrlSection **urlSecArr, int urlSecArrSize,int searchStart, int *index, UrlType type){


    for(int i=searchStart; i<urlSecArrSize ; i++){
        UrlSection *urlSec = urlSecArr[i];
        if(urlSec->type == type){
            if(index != NULL){
                *index = i;
            }
            return urlSec;
        }
    }
    return NULL;
}


char compareAbsoluteUrls(char *firstUrl, char *secondUrl){


    UrlSection **firstUrlSectionArr, **secondUrlSectionArr;
    int firstUrlSectionArrSize, secondUrlSectionArrSize;
    UrlType urlTypeArr [2] = {_DOMAIN_, _SECONDARY_DOMAIN_};
    char sameFlag = 1;

    if((firstUrlSectionArr = getUrlSectionsFromAbsoluteUrl(firstUrl,&firstUrlSectionArrSize)) == NULL){
        return 0;
    }
    if((secondUrlSectionArr = getUrlSectionsFromAbsoluteUrl(secondUrl,&secondUrlSectionArrSize)) != NULL){
        for(int i=0 ; i<2 ; i++){

            UrlSection *firstUrlSection = findUrlSectionByType(firstUrlSectionArr,firstUrlSectionArrSize,0,NULL,urlTypeArr[i]);
            UrlSection *secondUrlSection = findUrlSectionByType(secondUrlSectionArr,secondUrlSectionArrSize,0, NULL,urlTypeArr[i]);

            if(firstUrlSection == NULL || secondUrlSection == NULL){
                sameFlag=0;
                break;
            }
            if(strcmp(firstUrlSection->value, secondUrlSection->value) != 0){
                sameFlag=0;
                break;
            }
        }
    }
    freeStructArray((void ***) &firstUrlSectionArr, firstUrlSectionArrSize, freeUrlSection);
    freeStructArray((void ***) &secondUrlSectionArr, secondUrlSectionArrSize, freeUrlSection);


    return sameFlag;
}

char *convertUrlSectionArrToResourceLocator(UrlSection **urlSecArr, int start, int end, ResourceLocatorType rlType){

    char *resourceLocator = NULL;

    if(start>=end){
        return NULL;
    }

    for(int i=0 ; i<end ; i++){

        char *urlSecCpy =strCpyAlloc(urlSecArr[i]->value);

        if(rlType == _FS_PATH_) {
            if( i<end-1 ){
                strCatAlloc(&urlSecCpy, "d.", LEFT);
                strCatAlloc(&urlSecCpy, "/", RIGHT);
            } else {
                strCatAlloc(&resourceLocator, "f.", RIGHT);
            }
        } else {
            if(urlSecArr[i]->type == _PROTOCOL_){
                strCatAlloc(&urlSecCpy, "://", RIGHT);
            } else if(urlSecArr[i]->type == _SUB_DOMAIN_ || urlSecArr[i]->type == _SECONDARY_DOMAIN_) {
                strCatAlloc(&urlSecCpy, ".", RIGHT);
            } else if((urlSecArr[i]->type == _DOMAIN_ && i < end - 1) || (urlSecArr[i]->type == _PATH_ && i < end - 1 )){
                strCatAlloc(&urlSecCpy, "/", RIGHT);
            } else if(urlSecArr[i]->type == _QUERY_  ){
                strCatAlloc(&urlSecCpy, "?", LEFT);
            }
        }
        strCatAlloc(&resourceLocator, urlSecCpy, RIGHT);
        free(urlSecCpy);
    }
    if(rlType == _URL_) {
        rtrim(&resourceLocator, "?", RIGHT);
    }
    return resourceLocator;
}

char **convertUrlSectionArrToStringArr(UrlSection **urlSecArr, int urlSecArrSize){

    char **stringArr = NULL;

    if(urlSecArr == NULL || urlSecArrSize <= 0){
        return NULL;
    }
    if((stringArr = malloc(sizeof(char*) * urlSecArrSize)) == NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }

    for(int i=0 ; i<urlSecArrSize ; i++){
        stringArr[i] = urlSecArr[i]->value;
    }

    return stringArr;
}