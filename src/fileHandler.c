#define MESSAGE_BUFFER_SIZE 250
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>
#include <scrapper/stringUtils.h>
#include <stdarg.h>

#include "scrapper/fileHandler.h"
#include "scrapper/fileReader.h"
#include "scrapper/genericUtils.h"


FileIndex *initFileIndex(char *fileIndexPath){

    FileIndex *fileIndex;
    FILE *srcFile;


    if((fileIndex=malloc(sizeof(FileIndex)))==NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }
    fileIndex->fileArray=NULL;
    fileIndex->sizeArrayFile=0;

    if((srcFile=openFile(fileIndexPath,"r"))!=NULL) {

        if(!readFileIndex(&fileIndex, srcFile)){
            freeFileIndex((void**)&fileIndex);
        }
        fclose(srcFile);
    }
    else{
        createErrorReport("Opening index file error : ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }

    return fileIndex;
}


File *selectFile(FileIndex *fileIndex, char *searchedName){


    for(unsigned int i=0; i<fileIndex->sizeArrayFile;i++){

        if(strcmp(fileIndex->fileArray[i]->name,searchedName)==0){
            return fileIndex->fileArray[i];
        }
    }
    return NULL;
}



File *initFileElement(char *fileName, char *path, char *openMode,char *flux,OpenTime openTime){

    File *fileElement;

    if((fileElement=malloc(sizeof(File)))==NULL){
        createErrorReport("Memory allocation error : ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }

    if((fileElement->name=strCpyAlloc(fileName)) == NULL || (fileElement->path=strCpyAlloc(path))  == NULL ||
            (fileElement->openMode=strCpyAlloc(openMode))==NULL) {
        freeFileElement((void**)&fileElement);
        return NULL;
    }
    if(openTime==_NOW_){
        if(strcmp(flux,"null")!=0){
            if(strcmp(flux,"stderr")==0) {
                fileElement->filePointer=openFluxFile(fileElement->path,fileElement->openMode,stderr);
            }
            else if(strcmp(flux,"stdin")==0) {
                fileElement->filePointer=openFluxFile(fileElement->path,fileElement->openMode,stdin);
            }
            else if(strcmp(flux,"stdout")==0) {
                fileElement->filePointer=openFluxFile(fileElement->path,fileElement->openMode,stdout);
            }
        }
        else{
            fileElement->filePointer=openFile(fileElement->path,fileElement->openMode);
        }
    }
    return fileElement;
}

char freeFileIndex(void **untypedFileIndex){

    FileIndex *fileIndex = NULL;

    if(untypedFileIndex == NULL || *untypedFileIndex==NULL){
        return 0;
    }
    fileIndex = (FileIndex*)*untypedFileIndex;
    freeStructArray((void ***) &fileIndex->fileArray, fileIndex->sizeArrayFile, freeFileElement);

    free(fileIndex);
    fileIndex=NULL;

}

char freeFileElement(void **untypedFileElement){

    File *fileElement = NULL;
    if(untypedFileElement == NULL || *untypedFileElement==NULL){
        return 0;
    }
    fileElement = (File*)*untypedFileElement;

    if(fileElement->name!=NULL){
        free(fileElement->name);
    }
    if(fileElement->path!=NULL){
        free(fileElement->path);
    }
    if(fileElement->openMode!=NULL){
        free(fileElement->openMode);
    }
    if(fileElement->filePointer!=NULL){
        fclose(fileElement->filePointer);
    }
    free(fileElement);
    fileElement=NULL;

    return 1;
}



/*
 * ─── FONCTIONS D'OUVERTURE DE FICHIERS ───────────────────────────────────────────
 */

FILE *openFile(char *path, char *openMode){

    FILE *file;
    short bol=0;


    if((file=fopen(path,openMode))!=NULL){
        bol=1;
    }
    else if((file=fopen(path,"w+"))!=NULL){
        bol=1;
    }
    else{
        createErrorReport("File opening error : ",__FILE__,__LINE__,__DATE__,__TIME__);
    }
    if(bol){
        return file;
    }

    return NULL;
}


FILE *openFluxFile(char *path, char *openMode,  FILE * flux){

    FILE * redirectFile;
    short bol=0;

    if((redirectFile=freopen(path,openMode,flux))!=NULL){//freopen permet de rediriger un flux vers un fichier dans ce cas
        bol=1;
    }
    else if((redirectFile=freopen(path,"w+",flux))!=NULL){//Si ca ne fonctionne pas le mode w+ permet de creer le fichier
        bol=1;
    }
    else{
        createErrorReport("File opening error : ",__FILE__,__LINE__,__DATE__,__TIME__);
    }

    if(bol){
        return redirectFile;
    }
    return NULL;
}


/*
 * ─── FONCTIONS DE LECTURE DE FICHIERS ───────────────────────────────────────────
 */
unsigned long getFileSize(FILE *filePtr){
    unsigned long fileSize;
    fseek(filePtr,0,SEEK_END);
    fileSize=(unsigned long)ftell(filePtr);
    fseek(filePtr,0,SEEK_SET);

    return fileSize;
}

void createErrorReport(char *errorMessage,char * fileError, int lineError, char  *dateError, char *timeError, ...){

    char  *errorReportString = NULL;
    char  *addedErrorDetails = NULL;

    va_list params;

    if((errorReportString=malloc(sizeof(char) * (MESSAGE_BUFFER_SIZE *2 ))) == NULL || (addedErrorDetails = malloc(sizeof(char) * MESSAGE_BUFFER_SIZE)) == NULL){
        inlineFree(2, &errorReportString, &addedErrorDetails);
    }

    va_start (params,timeError);
    vsprintf(addedErrorDetails,errorMessage,params);
    va_end(params);

    if(sprintf(errorReportString,"| %s | file %s | line %d | date %s | time %s |",addedErrorDetails,fileError,lineError,dateError,timeError)>0){
        perror(errorReportString);//Perror permet 'envoie d'une erreur dans le flux stderr, le message d'erreur comprend le fichier, la ligne, la date et 'heure de 'erreur
    } else{
        fprintf(stderr,"Erreur lors de la creation d'un message d'erreur dans la fonction %s",__func__);
    }
    inlineFree(2, &errorReportString, &addedErrorDetails);
}
