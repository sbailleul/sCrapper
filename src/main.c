#include <stdlib.h>
#include <string.h>
#include <scrapper/multiThreadHandler.h>
#include <scrapper/fileReader.h>
#include "scrapper/fileHandler.h"

int main() {

    FileIndex *fileIndex= NULL;
    ScrapConfig * scrapConf = NULL;
    MultiThread * multiThread = NULL;
    FILE *scrapConfFile = NULL;
    FILE *uriHtmlTagListFile = NULL;
    int createThreadRes=0;

    if((fileIndex = initFileIndex("ressources/fileIndex.ini") )== NULL) {
        return EXIT_FAILURE;
    }
    scrapConfFile = selectFile(fileIndex,"scrapConfig")->filePointer;
    uriHtmlTagListFile = selectFile(fileIndex,"uriHtmlTagList")->filePointer;

    if(scrapConfFile == NULL || uriHtmlTagListFile == NULL || (scrapConf = initScrapConfig(scrapConfFile,uriHtmlTagListFile))== NULL) {
        freeFileIndex((void**)&fileIndex);
        return EXIT_FAILURE;
    }

    if((multiThread=initMultiThread(scrapConf,fileIndex)) == NULL){
        freeFileIndex((void**)&fileIndex);
        freeScrapConfig((void**)&scrapConf);
        return EXIT_FAILURE;
    }

    if((createThreadRes = pthread_create(&multiThread->multiThread,NULL, launchMultiThread,multiThread))){
        createErrorReport("Error n° = %d creating thread  :  ",__FILE__,__LINE__,__DATE__,__TIME__, createThreadRes);
        freeFileIndex((void**)&fileIndex);
        freeScrapConfig((void**)&scrapConf);
        freeMultiThread((void**)&multiThread);
        return EXIT_FAILURE;
    }

    mainLoop(multiThread);
    pthread_join(multiThread->multiThread, NULL);

    freeMultiThread((void**)&multiThread);
    freeScrapConfig((void**)&scrapConf);
    freeFileIndex((void**)&fileIndex);

    return EXIT_SUCCESS;
}













