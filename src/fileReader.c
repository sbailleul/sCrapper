//
// Created by sbailleul on 10/13/19.
//
#include <stdlib.h>
#include <string.h>
#include <scrapper/genericUtils.h>
#include "scrapper/fileHandler.h"
#include "scrapper/stringUtils.h"
#include "scrapper/scrapConfig.h"
#include "scrapper/fileReader.h"
#include "scrapper/urlHandler.h"



char readFileIndex(FileIndex **fileIndex, FILE *srcFile){

    char *row;
    char *seg;
    char propertiesArr[][2][FILE_BUFFER_SIZE]={{"name", ""}, {"path", ""}, {"openMode", ""}, {"flux", ""}};
    int propertiesCount=0;

    if((row=calloc(FILE_BUFFER_SIZE, sizeof(char))) == NULL || (seg=calloc(FILE_BUFFER_SIZE, sizeof(char))) == NULL){
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return 0;
    }

    while(fgets(row, FILE_BUFFER_SIZE, srcFile) != NULL){

        if((row = rtrim(&row," ",LEFT)) != NULL && row[0]!='#'){
            sscanf(row,"%[^=]=",seg);
            rtrim(&seg," ",ALL);

            if(propertiesCount>=3 && strncmp(seg,propertiesArr[0][0],strlen(propertiesArr[0][0]))==0){
                if(((*fileIndex)->fileArray=realloc((*fileIndex)->fileArray, ((*fileIndex)->sizeArrayFile+1)*sizeof(File*)))==NULL){
                    createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
                    return 0;
                }
                (*fileIndex)->fileArray[(*fileIndex)->sizeArrayFile] =
                        initFileElement(propertiesArr[0][1],propertiesArr[1][1],propertiesArr[2][1],propertiesArr[3][1],_NOW_);
                propertiesCount=0;
                (*fileIndex)->sizeArrayFile++;
            }

            for(int i=0 ; i<4; i++){
                if(strncmp(seg,propertiesArr[i][0],strlen(propertiesArr[i][0]))==0) {
                    sscanf(row + (strlen(seg) + 1), "%[^;]\n;", seg);
                    strcpy(propertiesArr[i][1], rtrim(&seg, "= ", ALL));
                    propertiesCount++;
                }
            }
        }
    }

    free(row);
    free(seg);

    if(propertiesCount>0){
        if(((*fileIndex)=realloc(*fileIndex, ((*fileIndex)->sizeArrayFile+1)*sizeof(File*))) == NULL){
            createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
            return 0;
        }
        (*fileIndex)->fileArray[(*fileIndex)->sizeArrayFile] =
                initFileElement(propertiesArr[0][1],propertiesArr[1][1],propertiesArr[2][1],propertiesArr[3][1],_NOW_);
        (*fileIndex)->sizeArrayFile++;
    }

    return 1;
}


char readScrapConfig(FILE *cnfFile, ScrapConfig *scrapConfig){

    char *row;
    int keyArrSize=0;
    Task *tsk;
    Action *act;
    fpos_t fpos;

    char ***keysValuesArr=NULL;

    if(cnfFile==NULL || (row=calloc(FILE_BUFFER_SIZE, sizeof(char))) == NULL ){
        return 0;
    }

    while(fgets(row, FILE_BUFFER_SIZE, cnfFile) != NULL && !feof(cnfFile)){

        if(rtrim(&row," ",ALL)[0]=='=' && row[1]!='='){
            getKeyValue(cnfFile, row, &keysValuesArr, &keyArrSize, &fpos);
            if((act=initAction(&keysValuesArr, &keyArrSize))!=NULL){
                if((scrapConfig->actionsArr=realloc(scrapConfig->actionsArr, sizeof(Action**) * (scrapConfig->actArrSize + 1))) == NULL){
                    createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
                }
                scrapConfig->actionsArr[scrapConfig->actArrSize]=act;
                scrapConfig->actArrSize++;
            }
            fsetpos(cnfFile,&fpos);
        }
        else if(!strncmp(rtrim(&row," ",ALL),"==",2)){
            getKeyValue(cnfFile, row, &keysValuesArr, &keyArrSize, &fpos);
            if((tsk=initTask(&keysValuesArr,NULL, &keyArrSize,0))!=NULL){
                if((scrapConfig->tasksArr=realloc(scrapConfig->tasksArr, sizeof(Task*) * (scrapConfig->tskArrSize + 1))) == NULL){
                    createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
                }
                scrapConfig->tasksArr[scrapConfig->tskArrSize]=tsk;
                scrapConfig->tskArrSize++;
            }
            fsetpos(cnfFile,&fpos);
        }
    }
    return 1;
}



char getKeyValue(FILE *pf,char *row, char ****keysValuesArr, int *size, fpos_t *fpos){

    char *key;
    char *val;
    int recursFreeSizeArr[]={2,*size};
    char **splitRowArr = NULL;
    int splitRowArrSize = 0;

    recursFree((void***)keysValuesArr,recursFreeSizeArr,2);
    *size=0;

    while(fgetpos(pf,fpos), fgets(row, FILE_BUFFER_SIZE, pf) != NULL && rtrim(&row, " ", ALL), row[0] != '='){

        if(rtrim(&row," ",ALL) != NULL && row[0]=='{' && rtrim(&row, "{",LEFT) != NULL){
            if(extractStringOnDelimiters(&row, "}",LEFT) != NULL){
                if((key=calloc(FILE_BUFFER_SIZE, sizeof(char))) == NULL || (val=calloc(FILE_BUFFER_SIZE, sizeof(char))) == NULL){
                    return 0;
                }
                if(strstr(row,"->")==NULL){
                    strcpy(key,"actions");
                    val = strCpyAlloc(row);
//                sscanf(row,"%*[{]%[^}]",val);
                } else {
                    splitRowArr = splitStringOnSeparator(row,&splitRowArrSize,"->",ALL,DESTROY);
                    if(splitRowArrSize == 2){
                        key = splitRowArr[0];
                        val = splitRowArr[1];
                    }
                    free(splitRowArr);
                }
                if((*keysValuesArr=realloc(*keysValuesArr,sizeof(char**)*(*size+1)))==NULL || ((*keysValuesArr)[*size]=calloc(2,sizeof(char*))) == NULL){
                    createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
                    return 0;
                }
                (*keysValuesArr)[*size][0]=key;
                (*keysValuesArr)[*size][1]=val;
                *size+=1;
            }
        }
        if(feof(pf)){
            break;
        }
    }
    return 1;

}


TagAttribute **readTagAttributeList(FILE *fp, int *tagAttributeArrSize) {

    TagAttribute **tagAttributeArr = NULL;
    char *buf;
    char **splitArr = NULL;
    int splitArrSize = 0;

    if ((buf = malloc(sizeof(char) * FILE_BUFFER_SIZE)) == NULL) {
        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
        return NULL;
    }
    while (fgets(buf, FILE_BUFFER_SIZE, fp) != NULL) {

        if (rtrim(&buf, " ", ALL) != NULL && buf[0] != '#'){
            if((splitArr = splitStringOnSeparator(buf, &splitArrSize, "=", ALL, DESTROY)) != NULL) {

                if ((tagAttributeArr = realloc(tagAttributeArr, sizeof(TagAttribute *) * ((*tagAttributeArrSize)+1))) == NULL) {
                    createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
                    recursFree((void ***) &splitArr, &splitArrSize, 1);
                    return NULL;
                }
                char *tag = rtrim(&splitArr[0]," \n",ALL);
                char *attribute = rtrim(&splitArr[1]," \n",ALL);
                tagAttributeArr[*tagAttributeArrSize] = initTagAttribute(tag, attribute);
                *tagAttributeArrSize+=1;
            }
        }

    }
    return tagAttributeArr;
}



char **getUrlArrFromHtmlDocument(FILE *fp, int *urlArrSize, TagAttribute **tagAttributeArr, int tagAttributeArrSize){

    char curChar;
    char **urlArr = NULL;
    char *tag = NULL;

    if(urlArrSize == NULL){
        return NULL;
    }

    *urlArrSize =0;
    fseek(fp,0,SEEK_SET);
    while(curChar=fgetc(fp), curChar!=EOF){

        if(curChar=='<'){

            fseek(fp,-1,SEEK_CUR);
            tag = getInlineTagFromHtmlDoc(fp);
            for(int i=0 ; i<tagAttributeArrSize ; i++){

                char *uriTagName = tagAttributeArr[i]->tagName;

                if(strncmp(tag+1,uriTagName, strlen(uriTagName))==0 && tag[strlen(uriTagName)+1] == ' ' && getUrlFromHtmlTag(&tag,tagAttributeArr[i]->attributeName)){
                    if((urlArr = realloc(urlArr,sizeof(char*) * (*urlArrSize + 1 ))) == NULL){
                        createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
                        free(tag);
                        return NULL;
                    }
                    removeCharsInString(tag," ");
                    urlArr[*urlArrSize]= strCpyAlloc(tag);
                    *urlArrSize += 1;
                }
            }
            free(tag);
        }
    }
    return urlArr;
}



char getUrlFromHtmlTag(char **tag, char *attributeName){

    char *url=NULL;
    char **tagArr=NULL;
    int splitArrSize=0;
    char *tmpUrl = NULL;
    int tmpUrlSize;
    char *newUrl = NULL;

    if((url=strstr(*tag,attributeName))!=NULL ){
        if((tmpUrl = strCpyAlloc(url)) == NULL){
            return 0;
        }
        if(extractStringOnDelimiters(&tmpUrl,"\"",LEFT) == NULL){
            return 0;
        }
        tmpUrlSize = tmpUrl != NULL ? strlen(tmpUrl): 0;
        if((newUrl = strCpyAlloc(url+tmpUrlSize)) == NULL){
            free(tmpUrl);
            return 0;
        }
        free(tmpUrl);
        rtrim(&newUrl," ",LEFT);
        if(newUrl[0]=='\"' && newUrl[1]!='\"'){
            if((tagArr=splitStringOnSeparator(newUrl,&splitArrSize,"\"",ALL,DESTROY))!=NULL){
                if(tagArr[0][0]!='#'){
                    free(newUrl);
                    strcpy(*tag,tagArr[0]);
                    recursFree((void***)&tagArr,&splitArrSize,1);
                    return 1;
                }
            }
        }
        free(newUrl);
    }
    return 0;
}


char *getInlineTagFromHtmlDoc(FILE *fp){


    char currentChar;
    char *tag=NULL;
    int tagSize=0;
    char tagSizeBonus=0;

    if(fp==NULL){
        return NULL;
    }
    while(currentChar=fgetc(fp), currentChar != EOF){

        if(currentChar != '\n'){
            tagSizeBonus = currentChar == '>' ? 2 : 1;
            if((tag = realloc(tag, sizeof(char) * (tagSize + tagSizeBonus)))==NULL){
                createErrorReport("Memory allocation error :  ",__FILE__,__LINE__,__DATE__,__TIME__);
                return NULL;
            }
            tag[tagSize] = currentChar;
            if(currentChar == '>'){
                tag[tagSize+1] = '\0';
                break;
            }
            tagSize+=tagSizeBonus;
        }
    }
    return tag;
}
