//*
// Created by sbailleul on 10/13/19.
//

#ifndef SCRAPPER_STRINGUTILS_H
#define SCRAPPER_STRINGUTILS_H


/**
 * @enum Direction : Used to set direction for string util functions, comportment depend of function.
 */
typedef enum Direction{LEFT,RIGHT,ALL}Direction;

/**
 * @enum SeparatorDirective : Used in splitStringOnSeparator function to keep separator or to destroy it when string is splitted
 */
typedef enum SeparatorDirective {KEEP,DESTROY} SeparatorDirective;

/**
 * @brief Delete trimChars from string will string founded chars are contained in trimChars string
 * @param string string to trim
 * @param trimChars chars to control
 * @param trimDir direction of trim action
 * @return trimed string pointer
 */
char * rtrim(char **string, char *trimChars, Direction trimDir);

/**
 * @brief Control if srcChar is contained in compChars
 * @param srcChar verified char
 * @param compChars list of char
 * @return return found char or 0
 */
char charsCmp(char srcChar, char *compChars);

/**
 * @brief Create an array of formats code contained in formats string
 * @param formats : String to split in array
 * @param formatSize : Pointer to array size
 * @return Return  format array on success, NULL on error
 */
char **getFormatArr(const char * formats, int *formatSize);


/**
 * @brief Split string in string array by separator, according to SeparatorDirective separator could be kept or destroyed.
 * According to Direction separator could be kept on the right or left of the string section
 * @param string : String to split
 * @param size size of created array
 * @param separator separator to split
 * @return return string array on success, NULL on error
 */
char **splitStringOnSeparator(char *string, int *size, const char *separator, Direction direction, SeparatorDirective directive);

/**
 * @brief Keep separator on split section of string, according to Direction repeat separator sequence on left side or right side of splitString string as much separatorCnt
 * @param substring
 * @param separator
 * @param direction
 * @param splitString
 * @param separatorCnt
 * @return
 */
char keepSeparatorOnSplitedString(char **substring, const char *separator, Direction direction, char *splitString, int separatorCnt);

/**
 * @brief Extract substring from string pointer on delimiters.
 * @param string : Target string for extraction
 * @param delimiters : List of delimiters to compare for each char of the string pointer
 * @param extractDir : LEFT, RIGHT or ALL value used to indicate if start counter  of extraction is incremented
 * or end counter of extraction is decremented will char doesn't match with one delimiter.
 * @return Return extracted substring
 */
char *extractStringOnDelimiters(char **string, char *delimiters, Direction extractDir);

/**
 * @brief Extract substring from string delimited by start and end value
 * @param string : Target string to extraction, reallocated if extraction work
 * @param start : Start of extraction
 * @param end : End of extraction
 * @return Return extracted string on success, NULL on error
 */
char *extractSubString(char **string, int start, int end);

/**
 * @brief Concatenate catString to dstString on LEFT or RIGHT side of dstString, reallocate dstString if necessary
 * @param dstString : Destination string of concatenation
 * @param catString : String to concatenate on destination string
 * @param dir : Direction of concatenation
 * @return Return destination string pointer on success, NULL on error
 */
char *strCatAlloc(char **dstString, char *catString, Direction dir);

/**
 * @brief Repeat string gift in parameter as much as count gift in parameter
 * @param string : String to repeat
 * @param count  : Repetition counter
 * @return Return repeated string on success, NULL on error
 */
char *repeatString(char *string, int count);

/**
 * @brief Merge string array in one dstString from start index to end index
 * @param stringArr : String array which contain string to concatenate in dstString
 * @param start : Start of array reading
 * @param end : End of array reading
 * @param dstString : DstString target of the merging if dstString isn't NULL string contained in stringArr are added to end of dstString
 * @return Return dstString pointer on success, NULL on error
 */
char * strMergeFromArr(char **stringArr, int start, int end, char **dstString);

/**
 * @brief Create a new string allocated which size of the srcString, after allocation srcString is copied in newString
 * @param srcString : Source string to copy
 * @return Return new source string on success, NULL on error
 */
char *strCpyAlloc(char *srcString);

/**
 * @brief Custom strtok to extract substring on delimiter, preserve integrity of origin string, and move foundPt on string for each call of function while wordSeparator is found
 * @param foundPt : Substring extract from current separator to next separator position
 * @param string : Target string of the function
 * @param wordSeparator : Separator which permit to cut the string
 * @param erasedChar : Char erased on foundPt substring on next wordSeparator position, this char is conserve to be reset and protect integrity of origin string
 * @param erasedPos : Position of next word separator, on this position a end of string char is set to terminate substring
 * @return Return consecutive wordSeparator counter found before foundPt string, 0 on string end
 */
int strTokWord(char **foundPt, char *string , char *wordSeparator, char *erasedChar, int *erasedPos);

/**
 * @brief Count consecutive how much time searchedString is consecutively found in string
 * @param string : Target string of the research
 * @param searchedString : Searched string in target string
 * @return Return counter of consecutive found searchedString
 */
int strStrConsecutiveString(char *string , char *searchedString);

/**
 * @brief Remove charsToRemove in string and reform the string to fill empty chars
 * @param string : String to transform
 * @param charsToRemove : Chars to remove
 * @return Return count of remove chars
 */
int removeCharsInString(char *string, char *charsToRemove);

/**
 * @brief Replace a word by an another as much time is found in target string
 * @param string : String to modify
 * @param wordToReplace : Word to find and replace by replaceWord
 * @param replaceWord  : Word which replace wordToReplace
 * @return Return 1 on  success
 */
char replaceWordByWord(char **string, char *wordToReplace, char *replaceWord);

/**
 * @brief Return total string size of stringArr, by looping it from start index to end index
 * @param stringArr : Array of string to loop
 * @param start : Start index for loop
 * @param end : End index for loop
 * @return Return aggregated size of all string stocked in stringArr from start index to end index on success, 0 on no result or error
 */
unsigned int getStringArrSize(char **stringArr, int start, int end);

/**
 * @brief Search a string stringToFind in string array stringArr by using string.h function strstr
 * @param stringToFind : String to search in stringArr
 * @param stringArr : Array of string looped to find stringToFind
 * @param stringArrSize : Size of stringArr
 * @return Return index of foundString on success, -1 on error or no result
 */
int findStringInStringArr(char *stringToFind, char **stringArr, int stringArrSize);

#endif //SCRAPPER_STRINGUTILS_H
