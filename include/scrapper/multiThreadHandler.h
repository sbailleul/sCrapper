//
// Created by sbailleul on 10/20/19.
//


#ifndef SCRAPPER_MULTITHREADHANDLER_H
#define SCRAPPER_MULTITHREADHANDLER_H

#include <pthread.h>
#include "scrapConfig.h"
#include "fileHandler.h"
#include "actionTree.h"

typedef struct TaskThread TaskThread;
typedef struct ActionThread ActionThread;
typedef struct MultiThread MultiThread;
typedef struct ActionThreadCentralizer ActionThreadCentralizer;
typedef struct Index Index;

/**
 * @struct Structure which execute configuration Action structure
 * @var actCentral : ActionThreadCentralizer structure pointer shared between all ActionThread of the same type
 * @var multiThread : MultiThread structure pointer
 * @var workFlag : Flag to check if this ActionThread is running.
 * @var timed : Flag to check if this ActionThread is timed by a TaskThread
 * @var actThread : pthread_t which represent a running thread
 */
struct ActionThread{

    ActionThreadCentralizer *actCentral;
    TaskThread *taskThread;
    MultiThread *multiThread;

    char workFlag;
    char timed;

    pthread_t actThread;
};

/**
 * @struct Struct which contain and share thread information for all sames ActionThreads
 * @var action : Action configuration structure
 * @var rootActTree : Entry for ActionTree structure tree
 * @var actTreeBucket : Bucket shared between all same type ActionThread this bucket stock ActionTree which are not be treated by ActionThread structures
 * @var versionInfo : VersionInfo structure pointer structure which is inited only if source Action structure have versionning option activated.
 * @var actThreadArr : ActionThread structure pointer array, permit to list all same type ActionThread launched.
 * @var actTreeBucketSize : Size of actTreeBucket incremented and decremented each time ActionThread pop or push ActionTree
 * @var actThreadArrSize : Size of actThreadArr
 * @var runningActThreadCnt : Counter of running ActionThread structure stocked in actThreadArr, value = actThreadArrSize where all threads run , value = actThreadArrSize * -1
 * @var endedActThreadsCnt : Counter of terminated ActionThread, if this counter == actThreadArrSize and Action have versionning info version logs will be write
 * @var transformActTreeMutex : Mutex to protect rootActTree and these children during its edition
 * @var transformBucketMutex : Mutex to protect actTreeBucket during push and pop of these elements
 * @var checkActThreadMutex : Mutex to protect runningActThreadCnt during its incrementation and decrementation
 * @var transformVersionInfoMutex : Mutex to protect shared VersionInfo pointer structure during its edition by ActionThreads
 * @var transformTimeMutex : Pointer to transformTimeMutex located in MultiThread structure protect tm  from time.h which isn't thread safe pointer.
 */
struct ActionThreadCentralizer{
    Action *action;
    ActionTree *rootActTree;
    ActionTree **actTreeBucket;
    VersionInfo* versionInfo;
    ActionThread **actThreadArr;

    unsigned long actTreeBucketSize;
    int actThreadArrSize;
    int runningActThreadCnt;
    int endedActThreadsCnt;

    pthread_mutex_t transformActTreeMutex;
    pthread_mutex_t transformBucketMutex;
    pthread_mutex_t checkActThreadMutex;
    pthread_mutex_t transformVersionInfoMutex;
    pthread_mutex_t* transformTimeMutex;

};

/**
 * @struct Struct which represent an ActionThread scheduler
 * @var task : Configuration Task structure which permit to get configuration information like time interval to trigger ActionThreads
 * @var multiThread : Pointer to MultiThread structure which permit to test if MultiThread is running
 * @var actionsThreadArr : Array of ActionThread structures used to launch action threads
 * @var instanceID : Instance current TaskThread, change if instance criteria is set in scrap configuration file
 * @var runFlag : Flag to check if TaskThread is running
 * @var secFrequency : Frequency of ActionThread trigger used if times criteria are set in scrap configuration file, else ActionThread are not timed
 * @var actionsThreadArrSize : Size of actionsThreadArr
 * @var runningActThreadCnt : Counter of running ActionThread located in actionsThreadArr if this counter == 0 TaskThread is close
 * @var currentTime : Recent time compared with last time to trigger action threads
 * @var lastTime : Old time compared with last time to trigger action threads
 * @var tskThreadRunIntervalCond : Condition to sleep action threads
 * @var actionThreadRunMutex : Mutex used for tskThreadRunIntervalCond
 * @var checkTaskThreadMutex : Mutex used to awake TaskThread if is not a time TaskThread structure
 * @var tskThread : Thread of the TaskThread
 */
struct TaskThread{

    Task *task;
    MultiThread *multiThread;
    ActionThread **actionsThreadArr;

    int instanceID;
    char runFlag;
    double secFrequency;
    int actionsThreadArrSize;
    int runningActThreadCnt;

    time_t currentTime;
    time_t lastTime;

    pthread_cond_t tskThreadRunIntervalCond;
    pthread_mutex_t actionThreadRunMutex;
    pthread_mutex_t checkTaskThreadMutex;
    pthread_t tskThread;
};

/**
 * @struct Struct which represent multi threading of scrapping, stop if all tasks are closed or if close signal is send by UI
 * @var actThreadsCentralArr : Array of ActionThreadCentralize shared between all same ActionThread type
 * @var taskThreadArr : Array of TaskThread structure pointer.
 * @var scrapConfig : ScrapConfig structure pointer used to  access html tag attribute list to extract urls from downloaded files
 * @var runFlag : Used to check running state of multi threading
 * @var taskThreadArrSize : Size of taskThreadArr
 * @var actThreadsCentralArrSize : Size of actThreadsCentralArr
 * @var runningTaskCnt : Counter of running TaskThread located in taskThreadArr if this counter == 0 runFlag is set to 0
 * @var transformTimeMutex : Mutex to protect tm structure pointer of time.h which is not thread safe pointer
 * @var checkMultiThreadEndMutex : Mutex to protect checking of multi threading running
 * @var endTaskThreadCntMutex : Mutex to protect edition of runningTaskCnt
 * @var endMultiThreadCond : Condition to awake and sleep multi thread instance to permit checking of its runFlag and close when == 0
 * @var multiThread : Multi threading instance stopped when all task are end or when user decide to close program
 */
struct MultiThread{

    ActionThreadCentralizer **actThreadsCentralArr;
    TaskThread **taskThreadArr;
    ScrapConfig *scrapConfig;

    char runFlag;
    int taskThreadArrSize;
    int actThreadsCentralArrSize;
    int runningTaskCnt;

    pthread_mutex_t transformTimeMutex;
    pthread_mutex_t checkMultiThreadEndMutex;
    pthread_mutex_t endTaskThreadCntMutex;
    pthread_cond_t endMultiThreadCond;
    pthread_t multiThread;

};

/**
 * @brief Menu which permit to user to close program
 * @param multiThread : MultiThread pointer structure, its runFlag will be set to 0 if user decide to close program
 * @return 1 on success 0 on error
 */
char mainLoop(MultiThread *multiThread);


/**
 * @brief Loop to trigger action thread run when time frequency is set to TaskThread or to wait trigger of action thread when they are terminated
 * @param tskThread : TaskThread structure pointer
 * @param runFlag : Running flag from MultiThread structure pointer
 * @return Return 1 on success 0 on error
 */
char handleActionsThreadTiming(TaskThread *tskThread, char *runFlag);

/**
 * @brief Get total size of task thread arr including instance criteria
 * @param taskArr : Task structure pointer array looped to detect instance criteria
 * @param tskArrSize : Size of taskArr
 * @return Return size of taskThreadArr on success or 0 on error
 */
int getTaskThreadArrSize(Task **taskArr, int tskArrSize);

/**
 * @brief Procedure that launches tasks and action threads, as well as the main loop (for console incoming interaction)
 * @param untypedMultiThread : a Multithread object that contains the ActionThreads and TaskThread
 */
void *launchMultiThread(void *untypedMultiThread);

/**
 * @brief Procedure that ends every running threads, thereby deallocating objects and writing logs
 * @param untypedMultiThread : a Multithread object that contains the ActionThreads and TaskThread
 */
char closeMultiThread(MultiThread *multiThread);

/**
 * @brief Procedure that launches tasks and action threads, as well as the main loop (for console incoming interaction)
 * @param untypedMultiThread : a Multithread object that contains the ActionThreads and TaskThread
 */
void *launchTaskThread(void *untypedTaskThread);

/**
 * @brief Procedure that launches tasks and action threads, as well as the main loop (for console incoming interaction)
 * @param untypedMultiThread : a Multithread object that contains the ActionThreads and TaskThread
 */
void *launchActionThread(void *untypedActionThread);

/**
 * @brief Procedure that ends every running threads, thereby deallocating objects and writing logs
 * @param untypedMultiThread : a Multithread object that contains the ActionThreads and TaskThread
 */
char terminateActionToLogs(ActionThreadCentralizer* actCentralizer, Task* tsk );


MultiThread *initMultiThread(ScrapConfig *scrapConfig, FileIndex *fileIndex);
TaskThread *initTaskThread(Task *task, MultiThread *multiThread, int instanceID);
ActionThread *initActionThread(ActionThreadCentralizer *actCentralizer, TaskThread *taskThread, MultiThread *multiThread);
ActionThreadCentralizer *initActionsCentralizer(Action *act, Task **tskArr, int tskArrSize, pthread_mutex_t* transformTimeMutex);

char freeActionThreadCentralizer(void **untypedCentralizer);
char freeMultiThread(void **untypedMultiThread);
char freeTaskThread(void **untypedTaskThread);
char freeActionThread(void **untypedActionThread);


#endif //SCRAPPER_MULTITHREADHANDLER_H
