//
// Created by sbailleul on 11/1/19.
//

#ifndef SCRAPPER_FILEWRITER_H
#define SCRAPPER_FILEWRITER_H

#include "scrapConfig.h"
#include <scrapper/actionThreadHandler.h>

typedef enum LogType{ACTION_LOG ,VERSION_LOG}LogType;
/**
 * @brief Split path by '/' delimiter and set directory in file system for all element of the generated path array,
 * except last element which is return as a file pointer
 * @param path  : Path to exploit
 * @return Return FILE pointer on success
 */
FILE *createFileDir(char *path);

/**
 * @brief Prepare string to be use in file system escape chars ' ' and '.'
 * @param string : String to escape
 * @return Return 1 on success
 */
char escapeStringToFileSystem(char ** string);


/**
 * @brief create file for action log and writes the first line if not exist, then write iteration informations
 * @param act : Action to write
 * @param ver : Version that contains infos
 * @return Return 1 on success
 */
FILE * initActionLog(Action* act);

/**
 * @brief cCreate file for this version of an action
 * @param act : Action to write
 * @param ver : Version that contains infos
 * @return Return 1 on success
 */
FILE *initVersionLog( Action* act, VersionInfo* ver);


/**
 * @brief writes the recap/footer to the VersionLog file
 * @param ver : VersionInfoStructure that contains the data to write
 * @param endTime : the time in which the action has ended (from the version info timestamps)
 * @param endDate : The date in which the action has ended (from the version info timestamps)
 * @return Return 1 on success
 */
char resumeVersionLog(VersionInfo *ver, char*  endTime, char* endDate );

/**
 * @brief Resumes the writes of the versionLog or actionLog, depending on the second argument
 * @param actCentral : ActionThreadCentralizer that contains the versionInfo and the tasks that are useful to write the log files
 * @param type : which type of log should be written : ACTION_LOG for actionLog, VERSION_LOG for versionLog
 * @return Return 1 on success
 */
char resumeWriteLog(ActionThreadCentralizer* actCentral, LogType type);

/**
 * @brief writes the recap/footer to the actionLog file
 * @param ver : VersionInfoStructure that contains the data to write
 * @param act : the action structure containing the name of the action, and the associated URL
 * @param startTime : the time in which the action has started (from the version info timestamps)
 * @param startDate : the date in which the action has started (from the version info timestamps)
 * @param endTime : the time in which the action has ended (from the version info timestamps)
 * @param endDate : The date in which the action has ended (from the version info timestamps)
 * @return Return 1 on success
 */
char resumeActionLog(VersionInfo *ver, Action *act, char *startTime, char *startDate, char*  endTime, char* endDate );
#endif //SCRAPPER_FILEWRITER_H
