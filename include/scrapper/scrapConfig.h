//
// Created by sbailleul on 10/12/19.
//
#ifndef SCRAPPER_CONFIG_H
#define SCRAPPER_CONFIG_H

#include <stdio.h>


typedef enum Key{MAX_DEPTH,VERSIONING,CONTENT_TYPES, SECONDS,MINUTES,HOURS,DAYS,ACTIONS,INSTANCE}Key;
typedef enum Type{INTEGER,STRING,REAL, CHARACTER}Type;

#define TIME_KEYS_ARR_SIZE 4
#define KEYS_ARR_SIZE 9
#define FORMAT_SIZE 20

typedef struct TagAttribute TagAttribute;
typedef union Value Value;
typedef struct Option Option;
typedef struct VersionInfo VersionInfo;
typedef struct Action Action;
typedef struct Task Task;
typedef struct ScrapConfig ScrapConfig;


/**
 * @struct TagAttribute : Structure which represent a html tag and html tag attribute
 * @var tagName : Tag name as input, select, a, img etc...
 * @var attributeName: Attribute of html tag as href, src etc...
 */
struct TagAttribute{
    char *tagName;
    char *attributeName;
};


/**
 * @union Value polymorphic types
 */
union Value{
    long integer;
    char *string;
    double real;
    char character;
};

/**
 * @struct Option represent action option
 * @var key offer easy access to control option type
 * @var value is a polymorphic type, integer, real, string or character
 */
struct Option{
    Key key;
    Value *value;
};

/**
 * @brief Task : Stores the infos concerning the version if the versionnign option is enabled
 * @var timeStamp : timestamp designating this version
 * @var totalFilesCount : The total amount of fetched files
 * @var CountPerTypeArr : Number of occurrences for each file type in a keyValue array
 * @var actionLogFile : points version Log txt for the whole action
 * @var actionLogFile : points version Log txt for this version
 */
struct VersionInfo{
    time_t startStamp;
    time_t endStamp;
    int totalFilesCount;
    int countPerTypeArrSize;
    char** typeKeysArray;
    int* typeCounterArray;
    int tskArrSize;
    FILE* actionLogFile;
    FILE* versionLogFile;
    Task **tskArr;

};

/**
 * @struct Action :  Represent action stocked in config file
 * @var name : Name of the action
 * @var url : Start url of the action
 * @var optionsArr : Stock all options of action
 * @var optArrSize : Size of optionsArr
 * @var versionInfo : infos of the version in this action
 */
struct Action{
    char *name;
    char *directory;
    char *url;
    Option **optionsArr;
    int optArrSize;
};

/**
 * @brief Task : Represent task stocked in config file
 * @var actionsArr: Actions array executed by the task
 * @var optionsArr : Options array to plan the task
 * @var actArrSize : Size of actionsArr and versionInfoArr
 * @var optArrSize : Size of optionsArr
 */
struct Task{
    char *name;
    Action **actionsArr;
    Option **optionsArr;
    int actArrSize;
    int optArrSize;
};

/**
 * @struct ScrapConfig : Contain all configuration of the programme
 * @var tasksArr :  Array of planning tasks
 * @var actionsArr : Array of actions used by tasks
 * @var tskArrSize : Size of tasksArr
 * @var actArrSize : Size of actionsArr
 */
struct ScrapConfig{
    Task **tasksArr;
    Action **actionsArr;
    TagAttribute **tagAttributeArr;
    int tagAttributeArrSize;
    int tskArrSize;
    int actArrSize;
};


/**
 * @brief Initialize ScrapConfig structure by reading the scrap
 * configuration file pointer given in parameter of the function
 * @param confFile : File pointer of configuration file
 * @return initialized ScrapConfig structure on success, NULL on error
 */
ScrapConfig *initScrapConfig(FILE *confFile, FILE *tagAttributeFile);

/**
 * @brief Initialize Action structure, by stocking information stocked in keysValuesArr
 * @param keysValuesArr : Array of key value pairs to collect Action properties
 * @param size : Size of keysValuesArr updated when information are found
 * @return initialized Action structure on success, NULL on error
 */
Action *initAction(char ****keysValuesArr, int *size);

/**
 * @brief Initialize Task structure, by stocking information stocked in keysValuesArr
 * @param keysValuesArr : Array of key value pairs to collect Task properties
 * @param actArr : Pointer to actionsArr
 * @param size : Size of keysValuesArr updated when information are found
 * @param actArrSize  : Size of actionsArr
 * @return initialized Task structure on success, NULL on error
 */
Task *initTask(char ****keysValuesArr,Action **actArr, int *size, int actArrSize);

/**
 * @brief Initialize Option structure, by getting information stocked in keysValuesArr.
 * @param keysValuesArr : Array of key and values
 * @param keysValuesArrSize : Size of the keysValuesArr
 * @return
 */
Option *initOption(char ****keysValuesArr, int *keysValuesArrSize);

/**
 * @brief Initialize Option pointer array, by getting information stocked in keysValuesArr.
 * @param keysValuesArr
 * @param size
 * @return
 */
Option **initOptionsArr(char ****keysValuesArr, int *keysValuesArrSize, int *optArrSize);

/**
 * @brief Init Value union pointer convert dynamically value found in keysValuesArr with formatKey
 * @param keysValuesArr : Array of key value
 * @param keysValuesArrSize : Size of keysValuesArr
 * @param formatKey : Format use to get converted value
 * @return Return Value pointer on success
 */
Value *initValue(char ****keysValuesArr, int *keysValuesArrSize, char *formatKey);


/**
 * @brief Read options arr in task to found ACTIONS key, if action option is found compare value with actions name to find corresponding action.
 * @param tasksArr : Array of Task pointer
 * @param actionsArr : Action pointer array
 * @param tskArrSize : Size of Task pointer array
 * @param actArrSize : Size of Action pointer array
 * @return Return count of affected Action pointers
 */
int setActionsToTasks(Task ***tasksArr, Action **actionsArr, int tskArrSize, int actArrSize);

/**
 * @brief Read Option pointer array to return first Option pointer which match with Key gift in parameter
 * @param key : Key value searched
 * @param optionArr : Option pointer array to read
 * @param optionArrSize : Option pointer array size
 * @return Return found option on success
 */
Option *getOptionByKey(Key key, Option **optionArr, int optionArrSize);

/**
 * @brief Init TagAttribute structure pointer if tagName and attributeName aren't NULL
 * @param tagName : Name of html tag to copy
 * @param attributeName : Name of html tag attribute to copy
 * @return Return allocated TagAttribute pointer on success, NULL on error.
 */
TagAttribute *initTagAttribute(char *tagName, char *attributeName);

/**
 * @brief Init Version info for an action
 * @param act : Action of which the version info is depending
 * @return Return allocated TagAttribute pointer on success, NULL on error.
 */
VersionInfo *initVersionInfo(Action* act, Task **tskArr, int tskArrSize);

/**
 * @brief Get the interval of execution out of a task's parsed criterias
 * @param optionArr : an array of options parsed during initScrapConfig
 * @return frequency of execution for the task, in seconds
 */
unsigned long getOptionsTimeFrequency(Option **optionArr, int optionArrSize );


char freeScrapConfig(void** untypedScrapConf);
char freeTask(void **untypedTsk);
char freeAction(void **untypedAct);
char freeOption(void **untypedOpt);
char freeValue(Value ** val,Type type);
char freeTagAttribute(void **untypedTagAttribute);
char freeVersionInfo(void**untypedVersionInfo);


#endif //SCRAPPER_CONFIG_H
