//
// Created by sbailleul on 10/28/19.
//

#ifndef SCRAPPER_GENERICUTILS_H
#define SCRAPPER_GENERICUTILS_H

/**
 * @brief Copy to var_list arguments pointer value stocked in keysValuesArr
 * @param keysValuesArr generic array who stock key and values to copy
 * @param sizeArrVal size pointer of keysValuesArr
 * @param formats given formats to cast void pointers of source and destinations values and match keys stocked in keysValuesArr
 * @param ... list of destination void pointers
 * @return
 */
int cpyMapValuesByFormats(char ****keysValuesArr, int *sizeArrVal, const char* formats, ...);

/**
 * @brief Cast srcVal by format code in dstVal void pointer
 * @param srcVal : Source value to cast by format code
 * @param dstVal : Destination generic void pointer destination of the cast
 * @param format : Format which permit to decide what cast to do
 * @return Return 1 on success
 */
int affectTypedValToVoidPointerByFormat(char *srcVal, void *dstVal, char *format);
/**
 * @brief Copy string to void pointer, allocate new string to void pointer and use function strcpy to make hard copy of srcVal
 * @param vDstVal destination string
 * @param srcVal  source string
 * @return return 1 on success, 0 on error
 */
char cpyStringToVoid(void ** vDstVal, char *srcVal);
/**
 * @brief Set all non NULL pointer of pntArr to array start and all NULL pointer to array end
 * @param pntArr array to reduce
 * @param size   current array size
 * @return count of non NULL pointers
 */
int reduceToStartHoledPntArr(void **pntArr, int size);
/**
 * @brief recursive function to free pointers array,
 * for each call of this function if i>=1 for loop iterate from sizeArr[i] to 0 and make recursive call,
 * address of pointer stocked in pointerArr and loopCounter - 1 are sent to new call
 * @param pntArr pointer array to free
 * @param sizeArr array who stock size of current pointer array, it is read from end to start
 * @param i index of sizeArr to read
 * @return
 */
int recursFree(void ***pntArr, int *sizeArr, int i);

/**
 * @brief Free a list of pointer, read va_list in loop limited by ptCounter
 * @param ptCounter : Number of pointer to free
 * @param ... : Void pointers to free
 * @return Return number of free elements
 */
int inlineFree(int ptCounter,...);

/**
 * @brief Merge dstArr and addedArr in dstArr, use elementsSize to dynamically reallocate dstArr
 * @param dstArr : Destination void pointer array of the merge
 * @param dstArrSize : Size of destination array
 * @param addedArr : Added void pointer array to destination array
 * @param addedArrSize : Added void pointer array size
 * @param elementsSize : Size of array element which will be reallocated
 * @return Return the newSize of the destination array
 */
int mergeArrays(void ***dstArr, int dstArrSize,  void *** addedArr, int addedArrSize, size_t elementsSize);

/**
 * @brief Switch position of array pointer elements from start index to end index
 * @param arr : Target array of void pointers
 * @param start : Start index of invert action
 * @param end : Limit of invert action
 * @return Return ...
 */
int invertArrOrder(void **arr, int start, int end);

/**
 * @brief Free structure pointer array by executing for each element freeFunction gift in parameter
 * @param arr : Array to free
 * @param arrSize : Size of array to free
 * @param freeFunction : Function which permit to free structure
 * @return Return 1 on success;
 */
char freeStructArray(void ***arr, int arrSize, char freeFunction(void**));

/**
 * @brief Pop last element of pointer array arr and resize pointer array by making a realloc with arrSize decremented and typeSize
 * @param arr : Target pointer array of pop
 * @param arrSize : Size of arr
 * @param typeSize : Size of pointer type stocked in arr
 * @return Return pointer on popped pointer on success, NULL on error or no result
 */
void *popElementFromPointerArray(void ***arr, unsigned long *arrSize, size_t typeSize);

/**
 * @brief Push pointer pushedElement to pointer array arr, resize arr by making realloc on incremented arrSize and typeSize
 * @param arr : Target array of push
 * @param arrSize : Size of arr
 * @param pushedElement : Element to push in arr
 * @param typeSize : Size of pointers stocked in arr
 * @return Return 1 on success, 0 on error or no result
 */
char pushElementInPointerArray(void ***arr, unsigned long *arrSize, void *pushedElement, size_t typeSize);

/**
 * @brief Search pointer searchedPt in pointer array ptArr by comparing its memory address to each pointers stocked in ptArr
 * @param ptArr : Pointer array looped to find searchedPt
 * @param ptArrSize : Size of ptArr
 * @param searchedPt : Pointer to find in ptArr
 * @return Return first pointer which corresponding with searchedPt on success, NULL on no result or error
 */
void *findPointerInPointerArray(void ** ptArr, int ptArrSize, void *searchedPt);

#endif //SCRAPPER_GENERICUTILS_H
