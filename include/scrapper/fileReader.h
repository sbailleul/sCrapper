//
// Created by sbailleul on 10/13/19.
//

#ifndef SCRAPPER_FILEREADER_H
#define SCRAPPER_FILEREADER_H

#define FILE_BUFFER_SIZE 1000

#include <stdio.h>
#include "fileHandler.h"
#include "scrapConfig.h"


/**
 * @brief Read fileIndex file to fill File array of FileIndex structure, a key value array permit to get File information
 * @param fileIndex : FileIndex structure to complete
 * @param srcFile : Index file to read
 * @return 0 on error
 */
char readFileIndex(FileIndex **fileIndex, FILE *srcFile);

/**
 * @brief Read scrap configuration file to complete a ScrapConfig structure, main loop analyse first chars of row
 * if "==" or '=' are reade a second loop in getKeyValue function will extract Action or Task information of the file
 * in a keysValuesArr
 * @param cnfFile : File to read
 * @param scrapConfig : ScrapConfig structure to complete
 * @return 0 on error
 */
char readScrapConfig(FILE *cnfFile, ScrapConfig *scrapConfig);

/**
 * @brief Read scrap config file while first chars of file row are different of "==" or '=', stock reade {keys -> values} in keysValuesArr,
 * update file cursor position for each read row
 * @param pf : File pointer of scrap config file
 * @param row : String to stock read row
 * @param keysValuesArr : Array of key values
 * @param size : Size pointer of keysValuesArr updated when a new information is stocked in keysValuesArr
 * @param fpos : File cursor position
 * @return  0 on error
 */
char getKeyValue(FILE *pf,char *row, char ****keysValuesArr, int *size, fpos_t *fpos);


/**
 * @brief Read html doc to get all url placed in property and tag witch match with TagAttribute stocked in TagAttribute pointer array
 * @param fp : File to read
 * @param urlArrSize : Size pointer of the url array
 * @param tagAttributeArr : TagAttribute pointer array
 * @param tagAttributeArrSize : Size of the TagAttribute pointer array
 * @return Return url array on success, NULL on error
 */
char **getUrlArrFromHtmlDocument(FILE *fp, int *urlArrSize, TagAttribute **tagAttributeArr, int tagAttributeArrSize);

/**
 * @brief Get html tag without take care about line feed
 * @param fp : File to read
 * @return Return html tag string on success, NULL on error
 */
char *getInlineTagFromHtmlDoc(FILE *fp);

/**
 * @brief Extract url in html tag according to attributeName gift in parameter
 * @param tag : Src tag to scan
 * @param attributeName : Attribute name which can contain url
 * @return Return url on success
 */
char getUrlFromHtmlTag(char **tag, char *attributeName);

/**
 * @brief Init TagAttribute pointer array by reading uriHtmlTagsList configuration file
 * @param fp : File to read
 * @param tagAttributeArrSize : Size of the TagAttribute pointer array
 * @return Return TagAttribute array on success, NULL on error
 */
TagAttribute **readTagAttributeList(FILE *fp, int *tagAttributeArrSize);

#endif //SCRAPPER_FILEREADER_H
