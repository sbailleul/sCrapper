//
// Created by sbailleul on 10/28/19.
//

#ifndef SCRAPPER_URLHANDLER_H
#define SCRAPPER_URLHANDLER_H

#include <curl/curl.h>


#define DOMAIN_TYPES_ARR_SIZE 3
#define DEEPNESS_ARR_SIZE 2
#define PROTOCOL_SIZE 10

/**
 * @enum UrlType : Enum which represent a part of an url, example bellow for url http://app.test.fr/video?id=3&format=mp4 :
 * @var _PROTOCOL_ : Network protocol as http, https, ftp. [http://]
 * @var _SUB_DOMAIN_ : Network sub domain. [app.]
 * @var _SECONDARY_DOMAIN_ : Network secondary domain. [test.]
 * @var _DOMAIN_ : Network domain. [fr.]
 * @var _PATH_ : Resource path. [/video]
 * @var _QUERY_ : Url parameter. [?id=3&format=mp4]
 */
typedef enum UrlType{_PROTOCOL_,_SUB_DOMAIN_, _SECONDARY_DOMAIN_, _DOMAIN_, _PATH_, _QUERY_} UrlType;

/**
 * @enum ResourceLocatorType : Enum permit to differentiate url of path
 * @var _URL_ : Url like : http://app.test.fr/video?id=3&format=mp4
 * @var _FS_PATH_ : File system path like : d.http/d.fr/d.test/d.app/d.video/f.id=3&format=mp4
 */
typedef enum ResourceLocatorType{_URL_, _FS_PATH_} ResourceLocatorType;

/**
 * @struct UrlSection : Structure which represent a part of an absolute url
 * @var value : Text of the url part
 * @var type : Type of the url part
 */
typedef struct UrlSection{
    char *value;
    UrlType type;
}UrlSection;

/**
 * @brief  Init a UrlSection structure pointer
 * @param value : Value copied in the UrlSection
 * @param urlType : Type of UrlSection
 * @return  Return UrlSection pointer on success, NULL on error
 */
UrlSection* initUrlSection(char *value, UrlType urlType);


/**
 * @brief  Format an url extracted in scrap configuration file
 * @param srcUrl : Url to format
 * @return  Return 1 on success,0 on error.
 */
char formatUrl(char **srcUrl);

/**
 * @brief  Extract an url string part from an absolute url
 * @param url : Origin url
 * @param what : CURLUPart enum to extract
 * @return  Return url part string on success, NULL on error
 */
char *getUrlPartFromAbsoluteUrl(char *url,CURLUPart what);

/**
 * @brief  Return an inited UrlSection pointer from an absolute url, work on url QUERY & PROTOCOL UrlType
 * @param url : Absolute url to scan
 * @param urlType : UrlType enum to choice which part to extract
 * @return  Return UrlSection pointer on success, NULL on error
 */
UrlSection *getUnsecablePartFromAbsoluteUrl(char *url, UrlType urlType);

/**
 * @brief  Scan an absolute url to extract all part of it in an UrlSection pointer array
 * @param url : Scanned absolute url
 * @param urlSecArrSize : Size of the inited UrlSection pointer array
 * @return  Return UrlSection pointer array on success, NULL on error
 */
UrlSection **getUrlSectionsFromAbsoluteUrl(char *url, int *urlSecArrSize);

/**
 * @brief Get UrlType DOMAIN, SECONDARY_DOMAIN and SUB_DOMAIN types from an absolute url, use '.' to select domains
 * @param url : Absolute url to scan
 * @param domainSectionsArrSize : Size of the generated UrlSection pointer array
 * @return Return an UrlSection pointer array on success, NULL on error
 */
UrlSection **getDomainsFromAbsoluteUrl(char *url, int *domainSectionsArrSize);

/**
 * @brief Get UrlType PATH from an absolute url, use '/' delimiter to select paths
 * @param url : Url to scan
 * @param pathSectionsArrSize : Size of the UrlSection pointer array
 * @return Return UrlSection pointer array on success, NULL on error
 */
UrlSection **getPathsFromAbsoluteUrl(char *url, int *pathSectionsArrSize);

/**
 * @brief Get deepness from an UrlSectionArr, deepness is incremented when UrlType of UrlSection structure is equal to PATH or SUB_DOMAIN
 * @param urlSecArr : UrlSection pointer array to read
 * @param urlSecArrSize  : Size of the UrlSection pointer array
 * @return Return calculated deepness
 */
int getDeepnessFromUrlSectionsArr(UrlSection **urlSecArr, int urlSecArrSize);


/**
 * @brief Invert domain UrlSection structures in an UrlSection pointer array
 * @param urlSecArr : UrlSection pointer array to scan
 * @param urlSecArrSize : Size of UrlSection pointer array
 * @return Return number of inverted UrlSection structures
 */
int invertDomainOrderInUrlSectionArr(UrlSection **urlSecArr, int urlSecArrSize );

/**
 * @brief From searchStart find first UrlSection pointer in UrlSection pointer array which match with UrlType first in parameter
 * @param urlSecArr : UrlSection pointer array to scan
 * @param urlSecArrSize : Size of UrlSection pointer array
 * @param searchStart : Index to begin search in array
 * @param index : Index of found UrlSection
 * @param type : UrlType to search
 * @return Return found UrlSection pointer on success, when nothing was found
 */
UrlSection *findUrlSectionByType(UrlSection **urlSecArr, int urlSecArrSize, int searchStart, int *index, UrlType type);

/**
 * @brief Convert an UrlSection pointer array to path or url according to ResourceLocatorType gift in parameter
 * @param urlSecArr : UrlSection pointer array to scan
 * @param start : Start of the UrlSection pointer array loop
 * @param end : End of the UrlSection pointer array loop
 * @param rlType : ResourceLocatorType to decide what kind of resource locator to return
 * @return String which represent path or url on success, NULL on error
 */
char *convertUrlSectionArrToResourceLocator(UrlSection **urlSecArr, int start, int end, ResourceLocatorType rlType);

/**
 * @brief Compare two URL in the form of strings by breaking them into sections
 * @param firstUrl : first url to compare
 * @param secondUrl : second url to compare
 * @return 0 if the URLS are different, 1 if they are the same
 */
char compareAbsoluteUrls(char *firstUrl, char *secondUrl);

/**
 * @brief transforms a relative URL from a website into an absolute URL to allow Curl to fetch it
 * @param curlUrl : instance of the Curl defined CURLU
 * @param srcUrl : an absolute URL of the same domain as the relative URL
 * @param relativeUrl : the relative URL to process
 * @return an absolute URL
 */
char *getAbsoluteUrlFromRelativeUrl(CURLU *curlUrl , char *srcUrl,  char *relativeUrl);

/**
 * @brief transforms an array of url section objects to an array of their stringed URL section
 * @param urlSecArr : the array of URL section objects
 * @return an array of string containing the url sections
 */
char **convertUrlSectionArrToStringArr(UrlSection **urlSecArr, int urlSecArrSize);


char freeUrlSection(void **untypedUrlSec);

#endif //SCRAPPER_URLHANDLER_H
