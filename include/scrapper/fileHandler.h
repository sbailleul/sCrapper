//
// Created by sbailleul on 10/13/19.
//

#ifndef SCRAPPER_FILEHANDLER_H
#define SCRAPPER_FILEHANDLER_H

#include <stdio.h>

/**
 * @enum OpenTime
 */
typedef enum OpenTime{_NOW_,_LATER_}OpenTime;

/**
 * @struct  File
 * @brief   Struct who contain information on file
 * @var     File::name name of file element
 * @var     File::path path to access file
 * @var     File::openMode open mode to access file
 * @var     File::filePointer pointer to exploit file
 */
typedef struct File{
    char *name;
    char *path;
    char *openMode;
    FILE *filePointer;
}File;

/**
 * @struct  FileIndex
 * @brief   Struct Handle File structs
 * @var     FileIndex::fileArray fileElement array who represent files
 * @var     FileIndex::sizeArrayFile size of fileElement array
 */
typedef struct FileIndex{

    File **fileArray;
    unsigned int sizeArrayFile;
}FileIndex;


/**
 * @brief Init File struct, open file if OpenTime == _NOW_, redirect flux to file if flux is specified
 * @param fileName name of File structure
 * @param path     file access path
 * @param openMode file open mode
 * @param flux     redirected flux if exist
 * @param openTime when file will be open
 * @return pointer of File structure, on error NULL value
 */
File *initFileElement(char *fileName, char *path, char *openMode,char *flux,OpenTime openTime);

/**
 * @brief Init FileIndex structure by reading fileIndex file for each file represented in fileIndex file call initFileElement
 * @param fileIndexPath  fileIndex file path
 * @return               FileIndex pointer, on error occurred return NULL
 */
FileIndex *initFileIndex(char *fileIndexPath);


/**
 * @brief Select a File structure in FileIndex File array by name
 * @param fileIndex FileIndex pointer
 * @param searchedName name of File structure to retrieve
 * @return
 */
File *selectFile(FileIndex *fileIndex, char *searchedName);


/**
 * @brief Free File structure, use fclose function to free FILE pointer
 * @param fileElement
 * @return
 */
char freeFileElement(void **fileElement);

/**
 * @brief Free FileIndex structure and call freeFileElement function for each File struct in its fileArray variable
 * @param fileIndex
 * @return
 */
char freeFileIndex(void **fileIndex);



/**
 * @brief Open file and control its opening
 * @param fileName
 * @param openMode
 * @return file pointer or NULL if error occurred
 */
FILE *openFile(char *fileName, char *openMode);

/**
 * @brief Open file and redirect flux to opened file, default comportment is to redirect stderr into opened file
 * @param path
 * @param openMode
 * @param flux
 * @return file pointer or NULL if error occurred
 */
FILE *openFluxFile(char *path, char *openMode, FILE * flux);

/**
 * @brief Get file size by moving file cursor to file end and getting its position
 * @param filePtr file pointer to access file
 * @return
 */
unsigned long getFileSize(FILE *filePtr);


/**
 * @brief Format below params and call perror function to generate error log to stderr
 * @param errorMessage personalized message
 * @param fileError src file of error
 * @param lineError line in src file of error
 * @param dateError date of error
 * @param timeError time of error
 */
void createErrorReport(char *errorMessage,char * fileError, int lineError, char  *dateError, char *timeError, ...);
#endif //SCRAPPER_FILEHANDLER_H
