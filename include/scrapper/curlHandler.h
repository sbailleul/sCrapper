#ifndef SCRAPPER_CURLHANDLER_H
#define SCRAPPER_CURLHANDLER_H

#include <curl/curl.h>
#include <wchar.h>
#include <scrapper/fileHandler.h>

/**
 * @struct CurlRequest structure exploited in curl connections
 * @var acceptedMimesArr
 * @var acceptedMimesArrSize
 * @var dstFile
 *
 */
typedef struct CurlRequest{
    char *url;
    char **acceptedMimesArr;
    int acceptedMimesArrSize;
    FILE *dstFile;
}CurlRequest;

/**
 * @brief Init a CurlRequest structure
 * @param url : Url to request
 * @param acceptedMimes : Optional MIME types accepted by request, splitted in and acceptedMimesArr by comma separator
 * @param dstFile : Destination FILE pointer of curl request
 * @return Return new CurlRequest structure pointer on success, NULL on error
 */
CurlRequest *initCurlRequest(char *url, char *acceptedMimes, FILE *dstFile);

/**
 * @brief Callback function used by curl in
 * @param ptr
 * @param size
 * @param nmemb
 * @param curlResponse
 * @return
 */
size_t curlToFile(void *ptr, size_t size, size_t nmemb, FILE *curlResponse);

/**
 * @brief Defines parameters for a Curl instance
 * @param curlRequest : a pointer towards a CurlRequest structure containing the infos to add to the curl handle
 * @param curl : a curl handle instance to which the headers are added
 * @param headers : a pointer to a curl structure defining the headers
 * @return 1 on success
 */
char curlSetOpt(CurlRequest *curlRequest, CURL *curl, struct curl_slist *headers);

/**
 * @brief Initialises a handle for a curl request, and executes it after defining its parameters.
 * @param curlRequest : a pointer towards a CurlRequest structure to execute
 * @return
 */
char* curlConnect(CurlRequest *curlRequest);


char freeCurlRequest(void **untypedCurlRequest);

#endif //SCRAPPER_CURLHANDLER_H
