//
// Created by sbailleul on 11/7/19.
//

#ifndef SCRAPPER_ACTIONTHREADHANDLER_H
#define SCRAPPER_ACTIONTHREADHANDLER_H

#include "actionTree.h"
#include "curlHandler.h"
#include "multiThreadHandler.h"

/**
 * @brief Increments the count for each element MIME type extract from curl request  in the versionInfo
 * @param fileType : Action of which the version info is depending
 * @param actCentralizer : Action of which the version info is depending
 * @return Return 1 on error, 0 on success.
 */
char incrementVersionInfoArrays( TaskThread *tskThread, ActionThreadCentralizer* actCentral, char* fileType, char* url);

/**
 * @brief Treat a curl response, get urls from file created by curl request, set its urls in ActionTree structure if they match with website url and if they aren't already in ActionTree
 * push created ActionTree structure pointer to ActionTree bucket.
 * @param curlRequest : CurlRequest structure pointer, used to get last requested url and curl result file
 * @param tagAttributeArr : TagAttribute pointer structure array used to extract url from curl destination file
 * @param tagAttributeArrSize : Size of tagAttributeArr
 * @param actCentral : ActionThreadCentralizer pointer used to get : root ActionTree structure; bucket ActionTree; mutex; information about Action structure of action thread.
 * @return Return a counter of added action tree on success.
 */
int addCurlResponseToActionTree(CurlRequest *curlRequest, TagAttribute **tagAttributeArr, int tagAttributeArrSize, ActionThreadCentralizer *actCentral);


/**
 * @brief Push or pop ActionTree structure pointer from actTreeBucket according to value of pushedActionTree pointer,
 * if its value is NULL function will pop pointer from bucket else function will push pointer in bucket
 * @param actTreeBucket
 * @param pushedActionTree
 * @param actTreeBucketSize
 * @param transformBucketMutex
 * @return Return popped ActionTree structure pointer on pop on success or NULL on error or pop no result
 */
ActionTree *transformActionTreeBucket( ActionTree ***actTreeBucket, ActionTree *pushedActionTree, unsigned long *actTreeBucketSize, pthread_mutex_t *transformBucketMutex);

/**
 * @brief Function which write curl destination file to file system, launch curl request to fill this file and add ActionTree structure on success.
 * @param actThread : ActionThread structure pointer is used to access ScrapConfig structure and parent TaskThread of current ActionThread
 * @param actTree : Popped ActionTree structure pointer to treat
 * @return Return 1 on success, 0 on no result or error
 */
char transformActionTree(ActionThread *actThread, ActionTree *actTree);

/**
 * @brief Function used to read curl response file to extract urls and add it to ActionTree structure, if versioning option is activated versionLog.txt of current ActionThread will be updated
 * @param actCentralizer : Used to access Action config structure, VersionInfo structure to versioning and mutex to protect work on shared memory
 * @param tskThread : Parent TaskThread of current ActionThread used to write log in action version if versioning is set
 * @param actTree : Root action tree of the action used to attach new ActionTree structures.
 * @param scrapConfig : Used to access TagAttribute structure pointer array to extract url from curlResultFile
 * @param curlResultFile ; Result file from curl request
 * @param requestedUrl : Requested url by curl used to check if url located in curlResultFile are on the same website of the action and to get absolute url to parse in ActionTree structure
 * @return Return 1 on success, 0 on error or no result
 */
char treatCurlResponse(ActionThreadCentralizer *actCentralizer, TaskThread *tskThread, ActionTree *actTree, ScrapConfig *scrapConfig, FILE *curlResultFile, char *requestedUrl );

/**
 * @brief Function which check if all same action thread targeting the same action are running, increment ActionThreadCentralizer runningActThreadCnt when curThreadWorkingFlag == 1,
 * decrement when curThreadWorkingFlag == 0, when ActionThreadCentralizer runningActThreadCnt == (ActionThreadCentralizer  actThreadArrSize * -1) all action threads are considered as stopped.
 * If ActionCentralizer has no action thread running parent TaskThread runningActThreadCnt is decremented, if TaskThread isn't timed action thread send broadcast signal to wake up its TaskThread
 * @param actThreadCentral : ActionThreadCentralize used centralize running action thread information
 * @param actThread : Current ActionThread structure which have workFlag checked
 * @param curThreadWorkingFlag : Flag which tell if running action thread is running, this flag is set by testing if action thread has an ActionTree structure to treat
 * @return Return endFlag = 0 if ActionThreadCentralizer has'nt action thread running or endFlag = 1
 */
char checkActThreadRunning(ActionThreadCentralizer *actThreadCentral, ActionThread *actThread, int curThreadWorkingFlag);

#endif //SCRAPPER_ACTIONTHREADHANDLER_H
