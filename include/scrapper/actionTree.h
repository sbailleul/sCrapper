//
// Created by sbailleul on 10/22/19.
//

#ifndef SCRAPPER_ACTIONTREE_H
#define SCRAPPER_ACTIONTREE_H

#include "urlHandler.h"
#include "scrapConfig.h"
typedef struct ActionTree ActionTree;

/**
 * @struct ActionTree : Structure which is node of linked tree
 * @var urlSection : Structure pointer which represent a part of an absolute url
 * @var actTreeArrSize : Size of actTreeArr
 * @var actTreeArr : Array of ActionTree structure pointers children
 * @var parentTree : Parent ActionTree structure pointer, its value is NULL if this ActionTree is the root of linked tree
 */
struct ActionTree{

    UrlSection *urlSection;
    unsigned long actTreeArrSize;
    ActionTree **actTreeArr;
    ActionTree *parentTree;
};

/**
 * @brief  Init an ActionTree structure
 * @param urlSection : Pointer to an UrlSection structure which represent part of an absolute url
 * @param parentTree : Pointer to parent ActionTree
 * @param actTreeArr : ActionTree array, if this pointer is NULL and actTreeArrSize is different of 0, an new ActionTree is allocated
 * @param actTreeArrSize : Size of its ActionTree array
 * @return  Return NULL on error or ActionTree pointer on success
 */
ActionTree *initActionTree( UrlSection *urlSection, ActionTree *parentTree, ActionTree **actTreeArr, unsigned long actTreeArrSize);


/**
 * @brief  Recursive function which generate an ActionTree linked list based on an absolute url represented by an UrlSection pointer array
 * @param parentActTree : Parent ActionTree structure pointer, for root ActionTree structures this parameter is NULL
 * @param lastTree : Last ActionTree pointer created  during the last iteration of this function.
 * @param urlSecArr : UrlSection pointer array which represent an absolute url
 * @param urlSecArrSize : Size of the UrlSection pointer array gift in parameter
 * @param maxDepth : Limit calls of this function if its value is greater than -1
 * @param iteration : Counter to get value in UrlSection array, it's incremented for each call of this function.
 * @return  Return the last ActionTree structure pointer on success or NULL on error.
 */
ActionTree* setActionTreeByUrl(ActionTree *parentActTree, ActionTree **lastTree, UrlSection **urlSecArr,  int urlSecArrSize, int maxDepth, int iteration);

/**
 * @brief  Get from current ActionTree pointer to is last parent ActionTree pointer UrlSection pointers to stock them in an UrlSection array
 * @param actTree : Start ActionTree pointer
 * @param urlSecArrSize : Size of the generated UrlSection pointer array
 * @return  Return UrlSection pointer array on success or NULL on error
 */
UrlSection  ** getUrlSectionArrFromActionTree(ActionTree *actTree, int *urlSecArrSize);

/**
 * @brief  Recursive function which find the last child ActionTree pointer from the current ActionTree pointer which match with the UrlSection pointer array gift in parameter.
 * @param actTree : Start ActionTree pointer for the first call of this function or child ActionTree pointer for next recursive calls.
 * @param urlSectionArr : UrlSection pointer array which be used to compare UrlSection pointer of ActionTree structures and find the biggest sequence of matching ActionTree structure pointers.
 * @param urlSectionArrSize : Size of the UrlSection pointer array
 * @param iteration : Counter which be incremented for each sub calls of this function.
 * @return  Return the last ActionTree pointer which have parents which match with the UrlSection array or NULL on error
 */
ActionTree *findDeepestActionTreeByUrl(ActionTree *actTree, UrlSection **urlSectionArr, int urlSectionArrSize, int *iteration);


/**
 * @brief Write an ActionTree linked list to an working directory gift in scrap configuration file. Create on the last ActionTree pointer and directory for precedent ActionTree pointers.
 * @param actTree : Current node of an ActionTree linked tree.
 * @param workingDir : Working directory to write ActionTree linked tree.
 * @return  Return a fill pointer generated for the last ActionTree pointer of the linked tree on success or NULL on error.
 */
FILE *writeActionTreeToFileSystem(ActionTree *actTree,char *workingDir);

/**
 * @brief Return file system path or url from an ActionTree structure by reading all of its parents
 * @param actTree : ActionTree structure pointer which represent a part of an url
 * @param workingDir : Working directory of Action, if workingDir is NULL function return url else return path
 * @return Return url or file system path on success or NULL on error
 */
char *getResourceLocatorByActionTree(ActionTree *actTree, char *workingDir);

/**
 * @brief Compare absolute url to current url to check if absolute url target the Action website.
 * Analyse absolute url to find last ActionTree corresponding of this url and add in its children non corresponding part of url
 * @param startActTree : Root ActionTree, this pointer is change in function by the last ActionTree structure pointer in url
 * @param currentUrl : Url requested by curl to get file which is the source file of extracted absoluteUrl.
 * @param absoluteUrl : Extracted url from fetched file extract from website with currentUrl
 * @param maxDepth : Max depth for scrapping value = -1 if no max-depth defined >=0 if max-depth defined
 * @return Return last ActionTree structure pointer created in function on success or NULL on error or no result
 */
ActionTree *addActionTreeByAbsoluteUrl(ActionTree *startActTree,char *currentUrl, char *absoluteUrl, int maxDepth);

/**
 * @brief Create action tree bucket which is a bucket shared by action thread to treat ActionTree structures and rootActTree wich
 * @param act
 * @param rootActTree
 * @param actTreeBucket
 * @param actTreeBucketSize
 * @return
 */
char  initActionTreeBucketAndActionTreeRoot(Action *act, ActionTree **rootActTree, ActionTree ***actTreeBucket, unsigned long *actTreeBucketSize);

/**
 * @brief Free untypedActTree and all of its children from memory
 * @param untypedActTree : Void pointer to ActionTree structure
 * @return Return 1 on success, on on error
 */
char freeActionTree(void **untypedActTree);

#endif //SCRAPPER_ACTIONTREE_H
